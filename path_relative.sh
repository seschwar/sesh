sesh_load path_absolute
sesh_load path_butfirst
sesh_load path_first
sesh_load path_validate


path_relative() # BASE PATH > RELATIVE_PATH
# Output the PATH relative to BASE (or PWD).
#
# Example: path_relative foo foo/bar/qux -> bar/qux
{
    local base="$1"
    local path="$2"
    path_validate "$base" "$path"

    local base_butfirst base_first path_butfirst path_first
    base_butfirst=$(path_absolute . "$base")
    path_butfirst=$(path_absolute . "$path")
    while test -n "$base_butfirst" && test -n "$path_butfirst"
    do
        base_first=$(path_first "$base_butfirst")
        path_first=$(path_first "$path_butfirst")
        if test "$base_first" != "$path_first"
        then
            break
        fi
        base_butfirst=$(path_butfirst "$base_butfirst")
        path_butfirst=$(path_butfirst "$path_butfirst")
    done
    while test -n "$base_butfirst"
    do
        base_butfirst=$(path_butfirst "$base_butfirst")
        path_butfirst=..${path_butfirst:+/$path_butfirst}
    done
    if test -z "$path_butfirst"
    then
        path_butfirst=.

    fi
    printf '%s\n' "$path_butfirst"
}
