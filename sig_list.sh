: ${__sig_list:=$(
    kill -l | command awk '
        BEGIN {
            printf "%s", "DEBUG,ERR,EXIT,ZERR"
        }
        {
            while (match($0, /[A-Z][A-Z][A-Z]+([+-]?[0-9]+)?/) > 0) {
                signal = substr($0, RSTART, RLENGTH)
                sub(/[A-Z][A-Z][A-Z]+([+-]?[0-9]+)?/, "", $0)
                sub(/^SIG/, "", signal)
                if (signal ~ /(^[0-9]*$|[+-][0-9]+$)/) {
                    continue
                }
                printf ",%s", signal
            }
        }
    '
)}


sig_list() # > SIGNAL
# List SIGNAL names.
{
    local IFS=,
    if test -n "$__sig_list"
    then
        printf '%s\n' $__sig_list
    fi
}
