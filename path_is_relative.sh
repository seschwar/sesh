sesh_load path_validate


path_is_relative() # PATH
# Test whether the PATH is relative.
{
    local path="$1"
    path_validate "$path"
    case "$path" in
        (/*)
            return 1
            ;;
        (*)
            return 0
            ;;
    esac
}
