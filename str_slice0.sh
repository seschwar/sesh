sesh_load str_slice1


str_slice0() # INDEX LENGTH (STRING...|< STRING) > SUBSTRING
# Output the SUBSTRING at the 0-based INDEX of LENGTH (or up to the
# end of the STRINGs if LENGTH is negative) in the STRINGs.
{
    local index="$1"
    local length="$2"
    shift 2

    str_slice1 "$(( "$index" + 1 ))" "$length" "$@"
}
