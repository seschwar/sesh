sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_OSERR=71


ex_oserr() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_OSERR after printing the error MESSAGE.
{
    err "$EX_OSERR" "$@"
}
