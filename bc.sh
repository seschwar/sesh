sesh_load tmp_file


: "${__bc_tmp:=$(tmp_file XXXXXXXX.bc)}"


bc() # [FILE...]
# Execute bc.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/bc.html
{
    POSIXLY_CORRECT=1 command bc ${__bc_l:+-l} -- "$__bc_tmp" "$@"
}
