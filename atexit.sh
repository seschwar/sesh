sesh_load sh_quote
sesh_load sig_handle


atexit() # COMMAND [ARGUMENT...]
# Register the COMMAND to be run when the shell exits.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/functions/atexit.html
{
    sig_handle "$(sh_quote "$@")" EXIT
}
