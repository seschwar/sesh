warn() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Printing the error MESSAGE.
#
# https://www.freebsd.org/cgi/man.cgi?query=warn&sektion=3
{
    local progname=${0##*/}
    if test "$#" -gt 0
    then
        printf "%s: %s\n" "$progname" "$*"
    else
        sed "s/^/$progname: /"
    fi >&2
}
