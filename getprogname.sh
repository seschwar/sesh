getprogname() # > PROGNAME
# https://www.freebsd.org/cgi/man.cgi?query=getprogname&sektion=3
{
    printf '%s\n' "${0##*/}"
}
