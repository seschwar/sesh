sesh_load magnet_exact_topic


magnet_btih() # MAGNET_URI > BITTORRENT_INFO_HASH
# Get the MAGNET_URI's exact topic's BITTORRENT_INFO_HASHes.
#
# https://en.wikipedia.org/wiki/BitTorrent_(protocol)
{
    magnet_exact_topic "$@" | sed -n 's/^urn:btih://p'
}
