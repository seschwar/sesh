user_name() # [USER...] > NAME
# Output the NAMEs of the USERs or the current user.
{
    if test "$#" -eq 0
    then
        id -nu
    else
        local user
        for user in "$@"
        do
            id -nu "$user"
        done
    fi
}
