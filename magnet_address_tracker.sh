sesh_load url_decode


magnet_address_tracker() # MAGNET_URI > ADDRESS_TRACKER
# Get the MAGNET_URI's ADDRESS_TRACKERs.
{
    local uri="$1"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed -n 's/^tr=//p' \
        | url_decode
}
