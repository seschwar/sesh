sh_is_readonly() # NAME
# Test whether the variable NAME is readonly.
{
    ! (
        exec 2> /dev/null
        eval "$1="
    )
}
