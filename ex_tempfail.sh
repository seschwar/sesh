sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_TEMPFAIL=75


ex_tempfail() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_TEMPFAIL after printing the error MESSAGE.
{
    err "$EX_TEMPFAIL" "$@"
}
