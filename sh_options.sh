sh_options() # > OPTIONS
# Output the current shell's OPTION flags.
#
# Only widely compatible OPTIONs are output so that they may be passed
# to practically any Bourne Shell.
{
    local options=

    local option
    for option in a b C e f i m n s u v x
    do
        case "$-" in
            (*$option*)
                options=$options$option
                ;;
        esac
    done
    if test -n "$options"
    then
        echo "$options"
    fi
}
