sh_unalias() # NAME
# Undefine a the shell alias NAME.
{
    local name="$1"
    unalias "$name"
}
