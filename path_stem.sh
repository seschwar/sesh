sesh_load path_last
sesh_load path_validate


path_stem() # PATH > FILENAME_STEM
# Output the STEM of the FILENAME of the PATH.
#
# Example: path_stem foo/bar.qux -> bar
{
    local path="$1"
    path=$(path_last "$path")
    case "$path" in
        (*.)
            ;;
        (?*.?*)
            path=${path%.*}
            ;;
        (*)
            ;;
    esac
    printf '%s\n' "$path"
}
