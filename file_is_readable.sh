file_is_readable() # (FILE...|< FILE)
# Test whether the FILEs are readable.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -r "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -r "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
