sesh_load fd_validate


fd_heredoc() # FD STRING [COMMAND [ARGUMENT...]]
# Redirect FD reading from STRING for self or COMMAND.
{
    local fd="$1"
    local string="$2"
    shift 2

    fd_validate "$fd"
    if test "$#" -eq 0
    then
        set -- exec
    fi
    eval "
\"\$@\" $fd<< EOF
\$string
EOF
    "
}
