file_is_writeable() # (FILE...|< FILE)
# Test whether the FILEs are writeable.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -w "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -w "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
