ere_select() # REGULAR_EXPRESSION (STRING...|< STRING) > STRING...
# Output STRINGs matching the extended REGULAR_EXPRESSION.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_04
{
    local regex="$1"
    shift 1

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | grep -E -- "$regex" || :
}
