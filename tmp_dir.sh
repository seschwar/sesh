sesh_load tmp


tmp_dir() # > PATH
# Create a temporary directory.
#
# The directory is automatically deleted on program termination.
{
    local template="${1:-XXXXXXXX}"
    mktemp -d -- "$__tmp/$template"
}
