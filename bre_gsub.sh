bre_gsub() # REGULAR_EXPRESSION REPLACEMENT (STRING...|< STRING) > STRING...
# Substitute all occurrences of the basic REGULAR_EXPRESSION in the
# STRING with the REPLACEMENT.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_03
{
    local regex="$1"
    local replacement="$2"
    shift 2

    regex=$(printf '%s' "$regex" | sed 's|/|\\/|g')
    replacement=$(printf '%s' "$replacement" | sed 's|/|\\/|g')
    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | sed "s/$regex/$replacement/g"
}
