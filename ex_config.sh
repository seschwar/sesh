sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_CONFIG=78


ex_config() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_CONFIG after printing the error MESSAGE.
{
    err "$EX_CONFIG" "$@"
}
