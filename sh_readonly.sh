sh_readonly() # NAME
# Make the variable NAME readonly.
{
    readonly "$1"
}
