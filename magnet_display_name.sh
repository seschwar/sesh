sesh_load url_decode


magnet_display_name() # MAGNET_URI > DISPAY_NAME
# Get the MAGNET_URI's DISPLAY_NAMEs.
{
    local uri="$1"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed -n 's/^dn=//p' \
        | url_decode
}
