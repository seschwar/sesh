file_is_sgid() # (FILE...|< FILE)
# Test whether the FILEs have their set-GID bit set.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -g "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -g "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
