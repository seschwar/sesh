sesh_load awk
sesh_load awk/month_number


torrent_created_at() # TORRENT > ISO8601
# Get the TORRENT file's creation ISO8601 datetime.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "GENERAL" && $1 == "Created" && $2 == "on:" && $3 != "Unknown" {
            sub(/^[^:]+: /, "")
            printf "%04d-%02d-%02dT%sZ\n", $5, month_number($2), $3, $4
        }
    '
}
