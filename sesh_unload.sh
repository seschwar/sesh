sesh_unload() # NAME...
# Undefine previously sourced NAMEs.
#
# The function doesn't unload a NAME's dependencies and therefore is
# unable to fully undo a previous sesh_load().
{
    local name
    for name in "$@"
    do
        case "${__sesh_load_sourced:-}" in
            ("$name")
                __sesh_load_sourced=
                ;;
            ("$name "*)
                __sesh_load_sourced=${__sesh_load_sourced#"$name "}
                ;;
            (*" $name "*)
                __sesh_load_sourced="${__sesh_load_sourced%" $name "*} ${__sesh_load_sourced#*" $name "}"
                ;;
            (*" $name")
                __sesh_load_sourced=${__sesh_load_sourced%" $name"}
                ;;
        esac
        unset -f "$name" || :
    done
}
