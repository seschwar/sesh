sesh_load sig_unignore
sesh_load sig_validate


sig_handle() # HANDLER SIGNAL...
# Add a HANDLER for the SIGNALs in the current (sub-)shell.
{
    local handler="$1"
    shift 1

    sig_validate "$@"
    local pid; pid=$(exec sh -c 'echo "$PPID"')
    local signal
    for signal in "$@"
    do
        eval "local handlers=\"\${__sig_handlers_${signal}_${pid}:-:}\""
        case "$handlers" in
            ("$handler"|"$handler;"*|*";$handler;"*|*";$handler")
                continue
                ;;
            (*)
                eval "__sig_handlers_${signal}_${pid}=\"$handler;\$handlers\""
                ;;
        esac
    done
    sig_unignore "$@"
}
