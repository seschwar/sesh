sesh_load path_last
sesh_load path_validate


path_extension() # PATH > FILENAME_EXTENSION
# Output the EXTENSION (excluding the ".") of the FILENAME of the PATH.
#
# Example: path_extension foo/bar.qux -> qux
{
    local path="$1"
    path=$(path_last "$path")

    stem=${path%.*}
    extension=${path##*.}

    case "$path" in
        (*.)
            path=
            ;;
        (?*.?*)
            path=${path##*.}
            ;;
        (*)
            path=
            ;;
    esac
    printf '%s\n' "$path"
}
