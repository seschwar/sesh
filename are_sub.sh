sesh_load awk


are_sub() # REGULAR_EXPRESSION REPLACEMENT (STRING...|< STRING) > STRING...
# Substitute the first occurrences of the AWK REGULAR_EXPRESSION in
# the STRING with the REPLACEMENT.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/awk.html#tag_20_06_13_04
{
    local regex="$1"
    local replacement="$2"
    shift 2

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | awk -v regex="$regex" -v replacement="$replacement" '{ sub(regex, replacement); print; }'
}
