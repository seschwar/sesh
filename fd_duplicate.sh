sesh_load fd_validate


fd_duplicate() # FD1 FD2 [COMMAND [ARGUMENT...]]
# Duplicate FD1 to FD2 for self or COMMAND.
{
    local fd1="$1"
    local fd2="$2"
    shift 2

    fd_validate "$fd1" "$fd2"
    if test "$#" -eq 0
    then
        set -- exec
    fi
    eval "\"\$@\" $fd1>&$fd2"
}
