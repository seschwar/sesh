sesh_load awk


torrent_name() # TORRENT > NAME
# Get the TORRENT file's NAME.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "GENERAL" && $1 == "Name:" {
            sub(/^[^:]+: /, "")
            print
        }
    '
}
