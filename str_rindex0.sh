sesh_load awk
sesh_load awk/rindex


str_rindex0() # SUBSTRING (STRING...|< STRING) > INDEX
# Output the 0-based INDEX (or -1) of the rightmost occurence of the
# SUBSTRING in the STRINGs.
{
    local substring="$1"
    shift 1

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | awk -v substring="$substring" '{ print rindex($0, substring) - 1 }'
}
