sesh_load awk


are_select() # REGULAR_EXPRESSION (STRING...|< STRING) > STRING...
# Ouptut STRINGs matching the AWK REGULAR_EXPRESSION.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/awk.html#tag_20_06_13_04
{
    local regex="$1"
    shift 1

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | awk -v regex="$regex" '$0 ~ regex'
}
