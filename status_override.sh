sesh_load fd_duplicate
sesh_load fd_write
sesh_load tmp_fifo


status_override() # FD ERE STATUS COMMAND [ARGUMENT...]
# Run the COMMAND but override its exit status with the STATUS if the
# extended regular expression ERE matches content on the file
# descriptor FD.
#
# Some commands don't use their exit status properly.  This function
# can help smooth over that.
{
    local fd="$1"
    local regex="$2"
    local status="$3"
    shift 3

    local command; command=$(tmp_fifo)
    fd_write "$fd" "$command" "$@" &
    local command_pid="$!"
    local tee; tee=$(tmp_fifo)
    fd_duplicate 1 "$fd" tee -- "$tee" < "$command" &
    local tee_pid="$!"
    grep -Eq -- "$regex" < "$tee" &
    local grep_pid="$!"

    wait "$command_pid" || local command_status="$?"
    wait "$tee_pid" || local tee_status="$?"
    wait "$grep_pid" || local grep_status="$?"
    if test "${grep_status:-0}" -ne 0
    then
        status=${command_status:-0}
    fi
    return "$status"
}
