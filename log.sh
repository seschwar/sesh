# https://www.freebsd.org/cgi/man.cgi?query=syslog&sektion=3
readonly LOG_EMERG=0
readonly LOG_ALERT=1
readonly LOG_CRIT=2
readonly LOG_ERR=3
readonly LOG_WARNING=4
readonly LOG_NOTICE=5
readonly LOG_INFO=6
readonly LOG_DEBUG=7


: ${__loglevel:=6}


get_log_level() # > LEVEL
{
    echo "$__loglevel"
}


set_log_level() # LEVEL
{
    __loglevel=$1
}


log_emerg() # (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    log "$LOG_EMERG" "$@"
}
alias log_emergency=log_emerg


log_alert() # (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    log "$LOG_ALERT" "$@"
}


log_crit() # (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    log "$LOG_CRIT" "$@"
}
alias log_critical=log_crit


log_err() # (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    log "$LOG_ERR" "$@"
}
alias log_error=log_err


log_warning() # (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    log "$LOG_WARNING" "$@"
}
alias log_warn=log_warning


log_notice() # (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    log "$LOG_NOTICE" "$@"
}


log_info() # (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    log "$LOG_INFO" "$@"
}


log_debug() # (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    log "$LOG_DEBUG" "$@"
}


log() # LEVEL (MESSAGE...|< MESSAGE) 2> MESSAGE
{
    if test "$1" -gt "$__loglevel"
    then
        return 0
    fi

    local level
    local time
    level=$1
    time=`date -u +%FT%TZ`
    shift 1

    case "$level" in
        (0)
            level=EMERGENCY
            ;;
        (1)
            level=ALERT
            ;;
        (2)
            level=CRITICAL
            ;;
        (3)
            level=ERROR
            ;;
        (4)
            level=WARNING
            ;;
        (5)
            level=NOTICE
            ;;
        (6)
            level=INFO
            ;;
        (7)
            level=DEBUG
            ;;
    esac

    local progname=${0##*/}
    if test "$#" -gt 0
    then
        printf '%s %s %s: %s\n' "$time" "$progname" "$level" "$*"
    else
        sed "s/^/$time $progname $level: /"
    fi >&2
}
