sesh_load torrent_privacy


torrent_is_public() # TORRENT
# Test whether the TORRENT file is public.
{
    local torrent="$1"

    test "$(torrent_privacy "$torrent")" = "Public torrent"
}
