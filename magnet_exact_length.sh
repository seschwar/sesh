sesh_load url_decode


magnet_exact_length() # MAGNET_URI > EXACT_LENGTH
# Get the MAGNET_URI's EXACT_LENGTHs.
{
    local uri="$1"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed -n 's/^xl=//p' \
        | url_decode
}
