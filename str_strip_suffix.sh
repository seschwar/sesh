str_strip_suffix() # SUFFIX (STRING...|< STRING) > STRING
# Strip the SUFFIX from the start of the STRINGs.
{
    local suffix="$1"
    shift 1

    if test "$#" -gt 0
    then
        local string
        for string in "$@"
        do
            printf '%s\n' "${string%"$suffix"}"
        done
    else
        local string
        while IFS= read -r string
        do
            printf '%s\n' "${string%"$suffix"}"
        done
    fi
}
