file_is_older() # REFERENCE (FILE...|< FILE)
# Test whether the FILEs are older than the REFERENCE file.
{
    local reference="$1"
    shift 1

    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test "$file" -ot "$reference"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test "$file" -ot "$reference"
            then
                return 1
            fi
        done
    fi
    return 0
}
