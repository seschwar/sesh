sesh_load awk
sesh_load awk/rindex


str_rindex1() # SUBSTRING (STRING...|< STRING) > INDEX
# Output the 1-based INDEX (or 0) of the rightmost occurrence of the
# SUBSTRING in the STRINGs.
{
    local substring="$1"
    shift 1

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | awk -v substring="$substring" '{ print rindex($0, substring) }'
}
