user_gids() # [USER...] > GIDS
# Output the GIDS of the USERs or the current user.
{
    if test "$#" -eq 0
    then
        id -G
    else
        local user
        for user in "$@"
        do
            id -G "$user"
        done
    fi
}
