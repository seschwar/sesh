ere_quote() # (STRING...|< STRING) > REGULAR_EXPRESSION
# Quote all special extended regular expression characters in the
# STRING resulting in a REGULAR_EXPRESSION exactly representing the
# STRING.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_04_03
{
    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | sed 's/[$(*+.?[\^{|]/\\&/g'
}
