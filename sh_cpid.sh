sh_cpid() # > PID
# Output the PID of the most recent background child command.
{
    echo "${!:-}"
}
