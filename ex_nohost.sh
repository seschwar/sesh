sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_NOHOST=68


ex_nohost() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_NOHOST after printing the error MESSAGE.
{
    err "$EX_NOHOST" "$@"
}
