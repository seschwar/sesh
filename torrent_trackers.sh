sesh_load awk


torrent_trackers() # TORRENT > TRACKER_URL
# Get the TORRENT file's TRACKER_URLs.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "TRACKERS" && !/^  Tier #[0-9]+$/ {
            sub(/^ +/, "")
            print
        }
    '
}
