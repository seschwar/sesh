sh_ppid() # > PID
# Output the current shell's parent PID.
{
    echo "$PPID"
}
