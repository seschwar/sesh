sesh_load awk


str_lindex0() # SUBSTRING (STRING...|< STRING) > INDEX
# Output the 0-based INDEX (or -1) of the leftmost occurrence of the
# SUBSTRING in the STRINGs.
{
    local substring="$1"
    shift 1

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | awk -v substring="$substring" '{ print index($0, substring) - 1 }'
}
