sesh_load awk


str_slice1() # INDEX LENGTH (STRING...|< STRING) > SUBSTRING
# Output the SUBSTRING at the 1-based INDEX of LENGTH (or up to the
# end of the STRINGs if LENGTH is negative) in the STRINGs.
{
    local index="$1"
    local length="$2"
    shift 2

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | awk -v index_="$index" -v length_="$length" '
        {
            if (length_ < 0) {
                print substr($0, index_)
            } else {
                print substr($0, index_, length_)
            }
        }
    '
}
