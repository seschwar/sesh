sesh_load fd_validate


fd_close() # FD [COMMAND [ARGUMENT...]]
# Close FD for self or COMMAND.
{
    local fd="$1"
    shift 1

    fd_validate "$fd"
    if test "$#" -eq 0
    then
        set -- exec
    fi
    eval "\"\$@\" $fd>&-"
}
