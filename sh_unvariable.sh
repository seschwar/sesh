sh_unvariable() # NAME
# Undefine the variable NAME.
{
    unset -v "$1"
}
