sesh_load bc
sesh_load bc/a
sesh_load bc/sqrt


echo '
    define arcsin(x) {
        return a(x / sqrt(1 - x ^ 2))
    }
' >> "$__bc_tmp"
