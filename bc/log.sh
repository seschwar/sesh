sesh_load bc
sesh_load bc/l


echo '
    define log(b, x) {
        auto i
        for (i = 0; x % b == 0; i++) {
            x /= b
        }
        if (x != 0) {
            i += l(x) / l(b)
        }
        return i
    }
' >> "$__bc_tmp"
