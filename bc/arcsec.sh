sesh_load bc
sesh_load bc/a
sesh_load bc/sqrt


echo '
    define arcsec(x) {
        return 2 * a(1) - a((1 / x) / sqrt(1 - (1 / x) ^ 2))
    }
' >> "$__bc_tmp"
