sesh_load awk


torrent_privacy() # TORRENT > PRIVACY
# Get the TORRENT file's PRIVACY field.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "GENERAL" && $1 == "Privacy:" {
            sub(/^[^:]+: /, "")
            print
        }
    '
}
