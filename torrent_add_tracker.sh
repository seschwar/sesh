sesh_load awk


torrent_add_tracker() # TORRENT TRACKER_URL
# Add the TRACKER_URL to the TORRENT file.
{
    local torrrent="$1"
    local tracker="$2"

    LC_ALL=C transmission-edit --add "$tracker" "$torrent" | awk '
        /^	Added/ {
            sub(/^	Added "/, "")
            sub(/".*/, "")
            print
        }
    '
}
