sesh_load url_decode


magnet_manifest_topic() # MAGNET_URI > MANIFET_TOPIC
# Get the MAGNET_URI's MANIFEST_TOPICs.
{
    local uri="$1"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed -n 's/^mt=//p' \
        | url_decode
}
