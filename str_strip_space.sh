sesh_load str_strip


str_strip_space() # (STRING...|< STRING) > STRING
# Strip all whitespace from both the start and the end of the
# STRINGs.
{
    str_strip '[:space:]' "$@"
}
