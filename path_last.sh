sesh_load path_validate


path_last() # PATH > LAST
# Output the LAST component of the PATH.
#
# Example: path_last foo/bar/qux -> qux
{
    local path="$1"
    path_validate "$path"
    basename -- "$path"
}
