sesh_load warn


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_USAGE=64


ex_usage() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_USAGE after printing the error MESSAGE.
{
    warn "$@"
    usage >&2 # TODO remove
    exit "$EX_USAGE"
}


usage() # > USAGE
{
    printf  'Usage: %s ' "${0##*/}"
    sed -n '
        /^[[:blank:]]*main[[:blank:]]*([[:blank:]]*)/ {
            s/^[[:blank:]]*main[[:blank:]]*([[:blank:]]*)[[:blank:]]*#[#[:blank:]]*//
            p
            q
        }
    ' < "$0"
}
