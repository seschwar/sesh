sesh_load magnet_exact_topic


magnet_ed2k() # MAGNET_URI > EDONKEY2000_HASH
# Get the MAGNET_URI's exact topic's EDONKEY2000_HASH.
#
# https://en.wikipedia.org/wiki/EDonkey_network#Hash_identification
{
    magnet_exact_topic "$@" | sed -n 's/^urn:ed2k://p'
}
