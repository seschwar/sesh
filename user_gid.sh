user_gid() # [USER...] > GID
# Output the GIDs of the USERs or the current user.
{
    if test "$#" -eq 0
    then
        id -g
    else
        local user
        for user in "$@"
        do
            id -g "$user"
        done
    fi
}
