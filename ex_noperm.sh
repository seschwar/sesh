sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_NOPERM=77


ex_noperm() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_NOPERM after printing the error MESSAGE.
{
    err "$EX_NOPERM" "$@"
}
