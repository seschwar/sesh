sesh_load url_decode


magnet_keyword_topic() # MAGNET_URI > KEYWORD_TOPIC
# Get the MAGNET_URI's KEYWORD_TOPICs.
{
    local uri="$1"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed -n 's/^kt=//p' \
        | url_decode
}
