sesh_load warn


fd_validate() # (FD...|< FD)
# Validate FDs.
{
    if test "$#" -gt 0
    then
        local fd
        for fd in "$@"
        do
            if ! expr "$fd" : '[[:digit:]]\{1,\}$' > /dev/null 2>&1
            then
                warn "invalid file descriptor: $fd"
                return 1
            fi
        done
    else
        local fd
        while IFS= read -r fd
        do
            if ! expr "$fd" : '[[:digit:]]\{1,\}$' > /dev/null 2>&1
            then
                warn "invalid file descriptor: $fd"
                return 1
            fi
        done
    fi
    return 0
}
