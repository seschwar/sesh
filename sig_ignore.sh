sesh_load sig
sesh_load sig_validate


sig_ignore() # SIGNAL...
# Ignore the SIGNALs in the current (sub-)shell.
{
    sig_validate "$@"
    local pid; pid=$(exec sh -c 'echo "$PPID"')
    local signal
    for signal in "$@"
    do
        eval "local handlers=\"\${__sig_handlers_${signal}_${pid}:-:}\""
        case "$handlers" in
            (*';SIG_IGN')
                ;;
            (*)
                eval "__sig_handlers_${signal}_${pid}=\"\$handlers;SIG_IGN\""
                ;;
        esac
    done
    __sig_trap
}
