file_is_tty() # (FILE...|< FILE)
# Test whether the FILEs are TTYs.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -t 0 < "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -t 0 < "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
