not() # COMMAND [ARGUMENT...]
# Negate the COMMAND's exit status code.
{
    ! "$@"
}
