sh_quote() # (STRING...|< STRING)
# Quote the STRINGs to prevent evaluation by the shell.
{
    if test "$#" -gt 0
    then
        # Using sesh's awk() would cause a mutual infinite recursion.
        command awk -- '
            BEGIN {
                q = "'\''"
                for (i = 1; i < ARGC; i++) {
                    gsub(q, q "\\" q q, ARGV[i])
                    ARGV[i] = q ARGV[i] q
                    if (i > 1) {
                        printf " "
                    }
                    printf "%s", ARGV[i]
                }
                exit
            }
        ' "$@" < /dev/null
    else
        printf "'"
        command sed "s/'/'\\\\''/g"
        printf "'"
    fi
}
