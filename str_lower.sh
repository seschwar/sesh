sesh_load str_translate


str_lower() # (STRING...|< STRING) > STRING
# Translate all characters in the STRINGs to lower case.
{
    str_translate '[:upper:]' '[:lower:]' "$@"
}
