path_join() # (PATH...|< PATH) > PATH
# Join the PATHs using the platform's directory separator.
#
# Example: path_join foo /bar qux -> foo/bar/qux
{
    local joined=''
    if test "$#" -gt 0
    then
        local path
        for path in "$@"
        do
            if test -n "$joined" &&
                test "${joined%/}" = "$joined" &&
                test -n "$path" &&
                test "${path#/}" = "$path"
            then
                joined=$joined/$path
            else
                joined=$joined$path
            fi
        done
    else
        while IFS= read -r path
        do
            if test -n "$joined" &&
                test "${joined%/}" = "$joined" &&
                test -n "$path" &&
                test "${path#/}" = "$path"
            then
                joined=$joined/$path
            else
                joined=$joined$path
            fi
        done
    fi
    printf '%s\n' "$joined"
}
