user_groups() # [USER...] > GROUPS
# Output the GROUPS of the USERs or the current user.
{
    if test "$#" -eq 0
    then
        id -Gn
    else
        local user
        for user in "$@"
        do
            id -Gn "$user"
        done
    fi
}
