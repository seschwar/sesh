sh_alias() # NAME VALUE
# Define the shell alias NAME with the VALUE.
{
    local name="$1"
    local value="$1"
    alias "$name=$value"
}
