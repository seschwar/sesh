sesh_load url_decode


magnet_exact_topic() # MAGNET_URI > EXACT_TOPIC
# Get the MAGNET_URI's EXACT_TOPICs.
{
    local uri="$1"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed -n 's/^xt=//p' \
        | url_decode
}
