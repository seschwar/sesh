sh_is_variable() # NAME
# Test whether NAME refers to a variable.
(
    exec 2> /dev/null
    set -u
    eval ": \"\${$1}\""
)
