sesh_load path_validate


path_butfirst() # PATH > REST
# Output all components of the PATH except the first.
#
# Example: path_butfirst foo/bar/qux -> bar/qux
{
    local path="$1"
    path_validate "$path"
    case "$path" in
        (/*)
            while
                case "$path" in
                    (/*)
                        :
                        ;;
                    (*)
                        false
                        ;;
                esac
            do
                path=${path#/}
            done
            ;;
        (*/*)
            path=${path#*/}
            while
                case "$path" in
                    (/*)
                        :
                        ;;
                    (*)
                        false
                        ;;
                esac
            do
                path=${path#/}
            done
            ;;
        (*)
            path=
            ;;
    esac
    printf '%s\n' "$path"
}
