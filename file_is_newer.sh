file_is_newer() # REFERENCE (FILE...|< FILE)
# Test whether the FILEs are newer than the REFERENCE file.
{
    local reference="$1"
    shift 1

    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test "$file" -nt "$reference"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test "$file" -nt "$reference"
            then
                return 1
            fi
        done
    fi
    return 0
}
