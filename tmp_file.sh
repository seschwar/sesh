sesh_load tmp


tmp_file() # [TEMPLATE] > PATH
# Create a temporary file.
#
# The file is automatically deleted on program termination.
{
    local template="${1:-XXXXXXXX}"
    mktemp -- "$__tmp/$template"
}
