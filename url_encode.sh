sesh_load awk
sesh_load awk/url_encode


url_encode() # (STRING...|< STRING) > ENCODED
# Encode the STRINGs with RFC 3986 percent-encoding.
#
# https://en.wikipedia.org/wiki/Percent-encoding
# https://tools.ietf.org/html/rfc3986
{
    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | LC_ALL=C awk '{ print url_encode($0) }'
}
