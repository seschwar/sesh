sh_script() # > PATH
# Output the PATH of the current shell script.
{
    case "$0" in
        (*/*)
            printf '%s\n' "$0"
            ;;
        (*)
            return 66
            ;;
    esac
}
