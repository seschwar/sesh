sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_IOERR=74


ex_ioerr() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_IOERR after printing the error MESSAGE.
{
    err "$EX_IOERR" "$@"
}
