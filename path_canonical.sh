sesh_load ex_software
sesh_load path_butfirst
sesh_load path_first
sesh_load path_validate


path_canonical() # PATH > CANONICAL_PATH
# Output the canonical PATH with extraneous dots and slashes clean up.
#
# Example: path_canonical foo/../bar//qux/. -> bar/qux
{
    local path="$1"
    path_validate "$path"
    local first=
    local rest="$path"
    path=
    while :
    do
        first=$(path_first "$rest")
        rest=$(path_butfirst "$rest")
        case "$path" in
            ('')
                case "$first" in
                    ('')
                        ex_software "path_canonical(): programming error in line $LINENO"
                        ;;
                    (.|..|?*)
                        path=$first
                        ;;
                esac
                ;;
            (/)
                case "$first" in
                    (''|/)
                        ex_software "path_canonical(): programming error in line $LINENO"
                        ;;
                    (.|..)
                        ;;
                    (?*)
                        path=$path$first
                        ;;
                esac
                ;;
            (.)
                case "$first" in
                    (''|/)
                        ex_software "path_canonical(): programming error in line $LINENO"
                        ;;
                    (.)
                        ;;
                    (..|?*)
                        path=$first
                        ;;
                esac
                ;;
            (..)
                case "$first" in
                    (''|/)
                        ex_software "path_canonical(): programming error in line $LINENO"
                        ;;
                    (.)
                        ;;
                    (..|?*)
                        path=$path/$first
                        ;;
                esac
                ;;
            (?*/..)
                case "$first" in
                    (''|/)
                        ex_software "path_canonical(): programming error in line $LINENO"
                        ;;
                    (.)
                        ;;
                    (..|?*)
                        path=$path/$first
                        ;;
                esac
                ;;
            (?*/?*)
                case "$first" in
                    (''|/)
                        ex_software "path_canonical(): programming error in line $LINENO"
                        ;;
                    (.)
                        ;;
                    (..)
                        path=${path%/*}
                        ;;
                    (?*)
                        path=$path/$first
                        ;;
                esac
                ;;
            (?*)
                case "$first" in
                    (''|/)
                        ex_software "path_canonical(): programming error in line $LINENO"
                        ;;
                    (.)
                        ;;
                    (..)
                        path=.
                        ;;
                    (?*)
                        path=$path/$first
                        ;;
                esac
                ;;
        esac
        if test -z "$rest"
        then
            break
        fi
    done
    printf '%s\n' "$path"
}
