sesh_load sh_quote
sesh_load tmp_file


: "${__awk_tmp:=$(tmp_file XXXXXXXX.awk)}"


awk() # ARGUMENT... < INPUT > OUTPUT
# Execute AWK.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/awk.html
{
    local OPTARG
    local OPTIND
    local OPTFLAG
    local awk='POSIXLY_CORRECT=1 command awk'
    local progfile=false

    if ! test -s "$__awk_tmp"
    then
        POSIXLY_CORRECT=1 command awk "$@"
        return "$?"
    fi
    while getopts F:f:v: OPTFLAG
    do
        case "$OPTFLAG" in
            (F)
                awk="$awk -F $(sh_quote "$OPTARG")"
                ;;
            (f)
                if ! "$progfile"
                then
                    awk="$awk -f $(sh_quote "$__awk_tmp")"
                    progfile=true
                fi
                awk="$awk -f $(sh_quote "$OPTARG")"
                ;;
            (v)
                awk="$awk -v $(sh_quote "$OPTARG")"
                ;;
            (--)
                break
                ;;
            (*)
                return 1
                ;;
        esac
    done
    shift "$(($OPTIND - 1))"
    awk="$awk --"
    if ! "$progfile" && test "$#" -gt 0
    then
        local program
        program="$(sed '/^[[:space:]]*$/d; /^[[:space:]]*#/d; s/^[[:space:]]*//;' < "$__awk_tmp") $1"
        shift 1
        awk="$awk $(sh_quote "$program")"
    fi
    while test "$#" -gt 0
    do
        awk="$awk $(sh_quote "$1")"
        shift 1
    done
    eval "$awk"
}
