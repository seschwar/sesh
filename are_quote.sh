sesh_load ere_quote


are_quote() # (STRING...|< STRING) > REGULAR_EXPRESSION
# Quote all special AWK regular expression characters in the STRING
# resulting in a REGULAR_EXPRESSION exactly representing the STRING.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/awk.html#tag_20_06_13_04
{
    ere_quote "$@"
}
