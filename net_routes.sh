sesh_load awk
sesh_load awk/dot_mask_prefix
sesh_load awk/hex2col
sesh_load awk/hex2dot
sesh_load awk/hex2int
sesh_load awk/hex_mask_prefix
sesh_load awk/hex_swap_bytes
sesh_load awk/normalize_ipv4
sesh_load awk/normalize_ipv6
sesh_load sh_is_command


net_routes() # > ADDRESS_FAMILY ADDRESS PREFIX_LENGTH METRIC GATEWAY INTERFACE
# Output network routes.
{
    {
        if sh_is_command ip
        then
            {
                ip -family inet -oneline route show
                ip -family inet6 -oneline route show
            } | awk '
                BEGIN {
                    print "inet", normalize_ipv4("127.0.0.1"), 8, 0, normalize_ipv4("0.0.0.0"), "lo"
                    print "inet6", normalize_ipv6("::1"), 128, 0, normalize_ipv6("::"), "lo"
                }
                {
                    if ($1 == "default") {
                        for (i = 2; i <= NF; i++) {
                            if ($i == "src") {
                                if ($(i + 1) ~ /:/) {
                                    $1 = "::/0"
                                } else {
                                    $1 = "0.0.0.0/0"
                                }
                            }
                        }
                    }
                    if ($1 ~ /:/) {
                        family = "inet6"
                    } else {
                        family = "inet"
                    }
                    slash = index($1, "/")
                    address = substr($1, 1, slash - 1)
                    metric = 0
                    prefix = substr($1, slash + 1)
                    if (family == "inet6") {
                        gateway = "::"
                    } else {
                        gateway = "0.0.0.0"
                    }
                    for (i = 2; i <= NF; i++) {
                        if ($i == "dev") {
                            interface = $(i + 1)
                        }
                        if ($i == "metric") {
                            metric = $(i + 1)
                        }
                        if ($i == "via") {
                            gateway = $(i + 1)
                        }
                    }
                    if (family == "inet6") {
                        address = normalize_ipv6(address)
                        gateway = normalize_ipv6(gateway)
                    } else {
                        address = normalize_ipv4(address)
                        gateway = normalize_ipv4(gateway)
                    }
                    print family, address, prefix, metric, gateway, interface
                }
            '
        fi
        if sh_is_command netstat && test "$(uname)" = Darwin
        then
            netstat -nrW | awk '
                NR > 2 && /^[^ 	]*:$/ {
                    section = $1
                    sub(/:$/, "", section)
                    getline
                    next
                }
                NF == 0 {
                    next
                }
                section == "Internet" && $3 !~ /L/ {
                    family = "inet"
                    if ($1 == "default") {
                        $1 = "0.0.0.0/0"
                    }
                    slash = index($1, "/")
                    if (slash == 0) {
                        address = $1
                        prefix = ""
                    } else {
                        address = substr($1, 1, slash - 1)
                        prefix = substr($1, slash + 1)
                    }
                    octets = split(address, _, /\./)
                    for (i = 0; i < 4 - octets; i++) {
                        address = address ".0"
                    }
                    address = normalize_ipv4(address)
                    if (prefix == "") {
                        prefix = 8 * octets
                    }
                    metric = 0
                    if ($3 ~ /G/) {
                        gateway = $2
                    } else {
                        gateway = "0.0.0.0"
                    }
                    gateway = normalize_ipv4(gateway)
                    interface = $6
                    print family, address, prefix, metric, gateway, interface
                }
                section == "Internet6" && $3 !~ /L/ {
                    family = "inet6"
                    if ($1 == "default") {
                        $1 = "::/0"
                    }
                    slash = index($1, "/")
                    if (slash == 0) {
                        address = $1
                        prefix = 128
                    } else {
                        address = substr($1, 1, slash - 1)
                        prefix = substr($1, slash + 1)
                    }
                    address = normalize_ipv6(address)
                    metric = 0
                    if ($3 ~ /G/) {
                        gateway = $2
                    } else {
                        gateway = "::"
                    }
                    gateway = normalize_ipv6(gateway)
                    interface = $4
                    print family, address, prefix, metric, gateway, interface
                }
            '
        fi
        if sh_is_command netstat && test "$(uname)" = Linux
        then
            {
                local af
                for af in '' 4 6
                do
                    if netstat "-${af}enrW" > /dev/null 2>&1
                    then
                        netstat "-${af}enrW"
                    fi
                done
            } | awk '
                BEGIN {
                    print "inet", normalize_ipv4("127.0.0.1"), 8, 0, normalize_ipv4("0.0.0.0"), "lo"
                    print "inet6", normalize_ipv6("::1"), 128, 0, normalize_ipv6("::"), "lo"
                }
                /^Kernel [^ ]+ routing table$/ {
                    section = $2
                    getline
                    next
                }
                NF == 0 {
                    next
                }
                section == "IP" {
                    family = "inet"
                    address = normalize_ipv4($1)
                    prefix = dot_mask_prefix($3)
                    metric = $5
                    gateway = normalize_ipv4($2)
                    interface = $8
                    print family, address, prefix, metric, gateway, interface
                }
                section == "IPv6" && $3 !~ /!/ {
                    family = "inet6"
                    slash = index($1, "/")
                    address = normalize_ipv6(substr($1, 1, slash - 1))
                    prefix = substr($1, slash + 1)
                    metric = $4
                    gateway = normalize_ipv6($2)
                    interface = $7
                    print family, address, prefix, metric, gateway, interface
                }
            '
        fi
        if test -f /proc/net/route
        then
            awk '
                BEGIN {
                    print "inet", normalize_ipv4("127.0.0.1"), 8, 0, normalize_ipv4("0.0.0.0"), "lo"
                }
                NR > 1 {
                    family = "inet"
                    address = normalize_ipv4(hex2dot(hex_swap_bytes($2)))
                    prefix = hex_mask_prefix(hex_swap_bytes($8))
                    metric = $7
                    gateway = normalize_ipv4(hex2dot(hex_swap_bytes($3)))
                    interface = $1
                    print family, address, prefix, metric, gateway, interface
                }
            ' < /proc/net/route
        fi
        if test -f /proc/net/ipv6_route
        then
            awk '
                {
                    family = "inet6"
                    address = normalize_ipv6(hex2col($1))
                    prefix = hex2int($2)
                    metric = hex2int($6)
                    gateway = normalize_ipv6(hex2col($5))
                    interface = $10
                    print family, address, prefix, metric, gateway, interface
                }
            ' < /proc/net/ipv6_route
        fi
    } | sort -u
}
