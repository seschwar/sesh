sesh_load torrent_add_tracker
sesh_load torrent_blacklisted_trackers
sesh_load torrent_hash
sesh_load torrent_public_trackers
sesh_load torrent_remove_tracker


torrent_update_trackers() # TORRENT
# Update address trackers in TORRENT file with public trackers.
{
    local torrent="$1"
    torrent_public_trackers "$(torrent_hash "$torrent")" | while IFS= read -r tracker
    do
        torrent_add_tracker "$torrent" "$tracker"
    done | awk -v torrent="$torrent" '{ printf "added %s to %s\n", $0, torrent }'
    torrent_blacklisted_trackers | while IFS= read -r tracker
    do
        torrent_remove_tracker "$torrent" "$tracker"
    done | awk -v torrent="$torrent" '{ printf "removed %s from %s\n", $0, torrent }'
}
