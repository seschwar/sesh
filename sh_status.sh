sh_status() # > STATUS
# Output the exit STATUS code of the most recent pipeline.
{
    echo "${?:-0}"
}
