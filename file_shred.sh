sesh_load file_exists
sesh_load sh_is_command
sesh_load warn


file_shred() # FILE...
# Try to securely erase the FILEs.
#
# This is done in order to prevent data recovery after the file has
# been unlinked from the file system.  Note that this function is best
# effort only.  Due to differences in the underlying storage this is
# not guaranteed to work reliably.
#
# The file is overwritten several times with either random data or a
# certain pattern.  Then the file may be zeroed and truncated.
# Finally it is unlinked.
{
    if rm -fP 2> /dev/null
    then
        rm -P -- "$@"
    elif sh_is_command scrub
    then
        scrub --force --no-signature --remove -- "$@"
    elif sh_is_command shred
    then
        shred --remove=unlink --zero -- "$@"
    elif sh_is_command srm
    then
        case "$(srm --help 2>&1 || :)" in
            (*THC*) # srm from https://www.thc.org
                srm -z -- "$@"
                ;;
            (*--openbsd*) # srm 1.2.9+ from http://srm.sourceforge.net
                srm --openbsd -- "$@"
                ;;
            (*--zero*) # srm on macOS 10.11-
                srm --zero -- "$@"
                ;;
            (*) # srm 1.2.8- from http://srm.sourceforge.net
                srm -- "$@"
                ;;
        esac
    elif sh_is_command wipe
    then
        wipe -fs -- "$@"
    else
        local file
        for file in "$@"
        do
            if ! file_exists "$file"
            then
                warn "file_shred(): no such file or directory: $file"
                return 66
            fi
            local blockcount; blockcount=$(($(LC_ALL=C wc -c < "$file") / 512 + 1))
            local i
            for i in 1 2 3
            do
                dd bs=512 count="$blockcount" if=/dev/urandom of="$file" 2> /dev/null
            done
            dd bs=512 count="$blockcount" if=/dev/zero of="$file" 2> /dev/null
            : > "$file"
        done
        rm -- "$@"
    fi
}
