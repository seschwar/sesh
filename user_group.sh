user_group() # [USER...] > GROUP
# Output the GROUPs of the USERs or the current user.
{
    if test "$#" -eq 0
    then
        id -gn
    else
        local user
        for user in "$@"
        do
            id -gn "$user"
        done
    fi
}
