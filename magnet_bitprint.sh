sesh_load magnet_exact_topic


magnet_bitprint() # MAGNET_URI > BITPRINT_HASH
# Get the MAGNET_URI's exact topic's BITPRINT_HASHes.
{
    magnet_exact_topic "$@" | sed -n 's/^urn:bitprint://p'
}
