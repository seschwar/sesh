file_is_symlink() # (FILE...|< FILE)
# Test whether the FILEs are symbolic links.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -L "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -L "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
