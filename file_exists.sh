file_exists() # (FILE...|< FILE)
# Test whether the FILEs exist.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -e "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -e "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
