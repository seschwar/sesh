sh_unexport() # NAME
# Unexport the variable NAME without unsetting it.
{
    local name="$1"
    eval "local value=\"\$$name\""
    unset "$name"
    eval "$name=\$value"
}
