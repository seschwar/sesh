sesh_load awk
sesh_load awk/col_mask_prefix
sesh_load awk/dot_mask_prefix
sesh_load awk/hex2col
sesh_load awk/hex2int
sesh_load awk/hex_mask_prefix
sesh_load awk/normalize_ipv4
sesh_load awk/normalize_ipv6
sesh_load net_route
sesh_load sh_is_command


net_interfaces() # > INTERFACE ADDRESS_FAMILY ADDRESS PREFIX_LENGTH
# Output network interfaces and their IP addresses.
{
    {
        if sh_is_command ifconfig
        then
            ifconfig | awk '
                /^[^ 	:]+/ {
                    interface = $1
                    sub(/(:[0-9]+)?:?$/, "", interface)
                }
                $1 == "inet" || $1 == "inet6" {
                    address = "0"
                    family = $1
                    prefix = 0
                    for (i = 1; i <= NF; i++) {
                        if ($i ~ /:/) {
                            colon = index($i, ":")
                            key = substr($i, 1, colon - 1)
                            value = substr($i, colon + 1)
                        } else {
                            key = $i
                            value = $(i + 1)
                        }
                        if (key == "inet") {
                            address = normalize_ipv4(value)
                        }
                        if (key == "inet6") {
                            sub(/%.*$/, "", address)
                            address = normalize_ipv6(value)
                        }
                        if (key ~ /([Mm][Aa][Ss][Kk]|[Pp][Rr][Ee][Ff][Ii][Xx])/) {
                            prefix = value
                        }
                    }
                    if (prefix ~ /^0[Xx][0-9A-Fa-f]+$/) {
                        prefix = hex_mask_prefix(prefix)
                    } else if (prefix ~ /:/ ){
                        prefix = col_mask_prefix(prefix)
                    } else if (prefix ~ /\./ ){
                        prefix = dot_mask_prefix(prefix)
                    }
                    print interface, family, address, prefix
                }
            '
        fi
        if sh_is_command ip
        then
            ip -oneline address show | awk '
                {
                    address = "0"
                    interface = $2
                    prefix = 0
                    for (i = 3; i <= NF; i++) {
                        if ($i == "inet" || $i == "inet6") {
                            family = $i
                            address = $(i + 1)
                            slash = index(address, "/")
                            prefix = substr(address, slash + 1)
                            address = substr(address, 1, slash - 1)
                        }
                    }
                    if (family == "inet6") {
                        address = normalize_ipv6(address)
                    } else {
                        address = normalize_ipv4(address)
                    }
                    print interface, family, address, prefix
                }
            '
        fi
        if test -f /proc/net/fib_trie
        then
            local addresses; addresses=$(awk '
                /^[^ 	]/ {
                    section = $1
                    sub(/:$/, "", section)
                }
                section == "Local" && $1 == "/32" && $2 == "host" && $3 == "LOCAL" {
                    print address
                }
                {
                    address = normalize_ipv4($2)
                }
            ' < /proc/net/fib_trie)
            net_route $addresses | awk '
                {
                    address = normalize_ipv4($7)
                    familiy = $1
                    interface = $6
                    prefix = $3
                    print interface, familiy, address, prefix
                }
            '
        fi
        if test -f /proc/net/if_inet6
        then
            awk '
                {
                    address = normalize_ipv6(hex2col($1))
                    family = "inet6"
                    interface = $6
                    prefix = hex2int($3)
                    print interface, family, address, prefix
                }
            ' < /proc/net/if_inet6
        fi
    } | sort -u
}
