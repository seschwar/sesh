sesh_load awk


torrent_remove_tracker() # TORRENT TRACKER_URL
# Remove the TRACKER_URL from the TORRENT file.
{
    local torrent="$1"
    local tracker="$2"

    LC_ALL=C transmission-edit --delete "$tracker" "$torrent" | awk '
        /^	Removed/ {
            sub(/^	Removed "/, "")
            sub(/".*/, "")
            print
        }
    '
}
