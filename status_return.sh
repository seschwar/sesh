status_return() # STATUS
# Return with the exit STATUS code.
{
    (exit "${1:-0}";)
}
