sesh_load atexit


# The directory holding all temporary files for the current script.
# It needs to be initialized globally because the functions below will
# be called in sub-shells with their own traps and variables, which
# would make the code "tmp=$(tmp_file)" always create and then
# immediately clean up a new temporary directory.
: ${__tmp:=$(mktemp -d -- "${TMPDIR:-/tmp}/sesh-tmp.XXXXXXXX")}


# Clean up the temporary directory on exit of the script.
atexit rm -fr -- "$__tmp"
