sesh_load path_validate


path_butlast() # PATH > REST
# Output all components of the PATH except the last.
#
# Example: path_butlast foo/bar/qux -> foo/bar
{
    local path="$1"
    path_validate "$path"
    dirname -- "$path"
}
