sesh_load path_validate


path_is_absolute() # PATH
# Test whether the PATH is absolute.
{
    local path="$1"
    path_validate "$path"
    case "$path" in
        (/*)
            return 0
            ;;
        (*)
            return 1
            ;;
    esac
}
