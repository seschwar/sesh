sesh_load sig_validate


sig_send() # SIGNAL PID...
# Send the SIGNAL to the PIDs.
{
    local signal="$1"
    shift 1

    sig_validate "$signal"
    if test "$#" -gt 0
    then
        kill "-$signal" -- "$@"
    fi
}
