sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_NOINPUT=66


ex_noinput() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_NOINPUT after printing the error MESSAGE.
{
    err "$EX_NOINPUT" "$@"
}
