str_translate() # SOURCE DESTINATION (STRING...|< STRING) > STRING
# Translate characters from SOURCE character set to DESTINATION
# character set in the STRINGs.
{
    local source="$1"
    local destination="$2"
    shift 2

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | tr -- "$source" "$destination"
}
