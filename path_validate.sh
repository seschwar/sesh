sesh_load warn


path_validate() # (PATH...|< PATH)
# Validate the PATHs.
{
    if test "$#" -gt 0
    then
        local path
        for path in "$@"
        do
            if test -z "$path"
            then
                warn "invalid path: $path"
                return 1
            fi
        done
    else
        local path
        while IFS= read -r path
        do
            if test -z "$path"
            then
                warn "invalid path: $path"
                return 1
            fi
        done
    fi
    return 0
}
