sh_is_command() # (NAME...|< NAME)
# Test whether the NAMEs refer to commands.
#
# Commands are either shell aliases, builtins, or functions or executables in PATH.
{
    if test "$#" -gt 0
    then
        local name
        for name in "$@"
        do
            if ! command -v "$name" > /dev/null
            then
                return 1
            fi
        done
    else
        local name
        while IFS= read -r name
        do
            if ! command -v "$name" > /dev/null
            then
                return 1
            fi
        done
    fi
    return 0
}
