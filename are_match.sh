sesh_load awk


are_match() # REGULAR_EXPRESSION (STRING...|< STRING)
# Test whether the AWK REGULAR_EXPRESSION matches the STRING.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/utilities/awk.html#tag_20_06_13_04
{
    local regex="$1"
    shift 1

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | awk -v regex="$regex" '$0 !~ regex { exit 1 }'
}
