str_strip_prefix() # PREFIX (STRING...|< STRING) > STRING
# Strip the PREFIX from the beginning of the STRINGs.
{
    local prefix="$1"
    shift 1

    if test "$#" -gt 0
    then
        local string
        for string in "$@"
        do
            printf '%s\n' "${string#"$prefix"}"
        done
    else
        local string
        while IFS= read -r string
        do
            printf '%s\n' "${string#"$prefix"}"
        done
    fi
}
