sesh_source sh_is_executable


sh_which() # (NAME...|< NAME)
# Output the locations of the NAMEs in PATH.
{
    if sh_is_executable which
    then
        if test "$#" -gt 0
        then
            command which -- "$@" 2> /dev/null
        else
            tr '\n' '\0' | xargs -0 which -- 2> /dev/null
        fi
    else
        if test "$#" -gt 0
        then
            local name
            for name in "$@"
            do
                local path
                path=$(
                    set +e
                    unalias "$name"
                    unset -f "$name"
                    command -v "$name"
                    :
                ) 2> /dev/null
                case "$path" in
                    (/*)
                        printf '%s\n' "$path"
                        ;;
                    (*)
                        return 1
                        ;;
                esac
            done
        else
            local name
            while IFS= read -r name
            do
                local path
                path=$(
                    set +e
                    unalias "$name"
                    unset -f "$name"
                    command -v "$name"
                    :
                ) 2> /dev/null
                case "$path" in
                    (/*)
                        printf '%s\n' "$path"
                        ;;
                    (*)
                        return 1
                        ;;
                esac
            done
        fi
    fi
}
