sh_export() # NAME
# Export the variable NAME.
{
    local name="$1"
    export "$name"
}
