sesh_load tmp_file


: "${__torrent_public_trackers:=$(tmp_file)}"


torrent_public_trackers() # HASH > TRACKER_URL
# Output publicly known TRACKER_URLs.
{
    local hash="$1"

    if ! test -s "$__torrent_public_trackers"
    then
        curl \
            --connect-timeout 10 \
            --fail \
            --location \
            --output - \
            --silent \
            --url https://newtrackon.com/api/all \
            --url https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all.txt \
            --url "https://torrentz2.is/announcelist_$(
                curl \
                    --connect-timeout 10 \
                    --fail \
                    --location \
                    --output - \
                    --silent \
                    --url "https://torrentz2.is/$hash" \
                | grep -o '/announcelist_[[:digit:]]\{1,\}' \
                | sed 's|^/announcelist_||'
            )" \
            | grep -Fvx '' \
            | sort -u \
            > "$__torrent_public_trackers"
    fi
    cat -- "$__torrent_public_trackers"
}
