sesh_load awk
sesh_load awk/col2hex
sesh_load awk/hex2col


printf '%s\n' '
    function normalize_ipv6(ipv6_address) {
        return hex2col(col2hex(ipv6_address))
    }
' >> "$__awk_tmp"
