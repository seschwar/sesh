sesh_load awk


printf '%s\n' '
    function url_decode(string, _, i, len) {
        len = length(string)
        for (i = 1; i <= len; i++) {
            if (substr(string, i, 1) == "%") {
                string = sprintf("%s%c%s", substr(string, 1, i - 1), int("0x" substr(string, i + 1, 2)), substr(string, i + 3))
                len -= 2
            }
        }
        return string
    }
' >> "$__awk_tmp"
