sesh_load awk
sesh_load awk/dot2hex
sesh_load awk/hex2bin


printf '%s\n' '
    function dot2bin(dotted_decimal) {
        return hex2bin(dot2hex(dotted_decimal))
    }
' >> "$__awk_tmp"
