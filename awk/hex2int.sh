sesh_load awk


printf '%s\n' '
    function hex2int(hexadecimal) {
        sub(/^(0[Xx])?0*/, "0x", hexadecimal)
        return int(hexadecimal)
    }
' >> "$__awk_tmp"
