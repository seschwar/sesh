sesh_load awk
sesh_load awk/dot2col
sesh_load awk/left_pad


printf '%s\n' '
    function col2hex(colonned_hexadecimal, _, i, hexadecimal, hextets, len, replacement) {
        if (colonned_hexadecimal ~ /\./) {
            len = split(colonned_hexadecimal, hextets, /:/)
            colonned_hexadecimal = ""
            for (i = 1; i <= len; i++) {
                if (hextets[i] ~ /\./) {
                    hextets[i] = dot2col(hextets[i])
                    sub(/^[0:]+/, "", hextets[i])
                }
                if (i > 1) {
                    colonned_hexadecimal = colonned_hexadecimal ":"
                }
                colonned_hexadecimal = colonned_hexadecimal hextets[i]
            }
        }
        if (colonned_hexadecimal ~ /^::/ ) {
            colonned_hexadecimal = "0" colonned_hexadecimal
        }
        if (colonned_hexadecimal ~ /::$/ ) {
            colonned_hexadecimal = colonned_hexadecimal "0"
        }
        if (colonned_hexadecimal ~ /::/) {
            len = split(colonned_hexadecimal, hextets, /:/)
            replacement = ":"
            for (i = 1; i <= 8 - len + 1; i++) {
                replacement = replacement "0:"
            }
            sub(/::/, replacement, colonned_hexadecimal)
        }
        len = split(colonned_hexadecimal, hextets, /:/)
        hexadecimal = ""
        for (i = 1; i <= len; i++) {
            hexadecimal = hexadecimal left_pad(hextets[i], 4, "0")
        }
        sub(/^0*/, "0x", hexadecimal)
        if (hexadecimal == "0x") {
            hexadecimal = "0x0"
        }
        return hexadecimal
    }
' >> "$__awk_tmp"
