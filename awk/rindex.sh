sesh_load awk


printf '%s\n' '
    function rindex(string, substring, _, i, len) {
        len = length(substring)
        for (i = length(string) - len + 1; i >= 1; i--) {
            if (substr(string, i, len) == substring) {
                break
            }
        }
        return i
    }
' >> "$__awk_tmp"
