sesh_load awk
sesh_load awk/ip2bin


printf '%s\n' '
    function test_route(address, destination, prefix) {
        if ((address ~ /:/) != (destination ~ /:/)) {
            return 0
        }
        address = ip2bin(address)
        destination = ip2bin(destination)
        return substr(address, 1, prefix) == substr(destination, 1, prefix)
    }
' >> "$__awk_tmp"
