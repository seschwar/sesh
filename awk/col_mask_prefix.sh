sesh_load awk
sesh_load awk/bin_mask_prefix
sesh_load awk/col2bin


printf '%s\n' '
    function col_mask_prefix(address) {
        return bin_mask_prefix(col2bin(address))
    }
' >> "$__awk_tmp"
