sesh_load awk
sesh_load awk/left_pad


printf '%s\n' '
    function hex2dot(hexadecimal, _, dotted_decimal, i, len) {
        sub(/^0[Xx]/, "", hexadecimal)
        len = length(hexadecimal)
        hexadecimal = left_pad(hexadecimal, len + (2 - len % 2) % 2, "0")
        len += (2 - len % 2) % 2
        dotted_decimal = ""
        for (i = 1; i <= len; i += 2) {
            if (i > 1) {
                dotted_decimal = dotted_decimal "."
            }
            dotted_decimal = dotted_decimal sprintf("%u", "0x" substr(hexadecimal, i, 2))
        }
        if (dotted_decimal == "") {
            dotted_decimal = "0.0.0.0"
        }
        return dotted_decimal
    }
' >> "$__awk_tmp"
