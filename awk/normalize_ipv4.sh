sesh_load awk


printf '%s\n' '
    function normalize_ipv4(ipv4_address, _, i, len, octets) {
        len = split(ipv4_address, octets, /\./)
        ipv4_address = ""
        for (i = 1; i <= len; i++) {
            if (i > 1) {
                ipv4_address = ipv4_address "."
            }
            sub(/^0+/, "", octets[i])
            if (octets[i] == "") {
                octets[i] = "0"
            }
            ipv4_address = ipv4_address octets[i]
        }
        return ipv4_address
    }
' >> "$__awk_tmp"
