sesh_load awk
sesh_load awk/bin2hex
sesh_load awk/hex2col


printf '%s\n' '
    function bin2col(binary) {
        return hex2col(bin2hex(binary))
    }
' >> "$__awk_tmp"
