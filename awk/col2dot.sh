sesh_load awk
sesh_load awk/col2hex
sesh_load awk/hex2dot


printf '%s\n' '
    function col2dot(colonned_hexadecimal) {
        return hex2dot(col2hex(colonned_hexadecimal))
    }
' >> "$__awk_tmp"
