sesh_load awk
sesh_load awk/left_pad


printf '%s\n' '
    function bin2hex(binary, _, hexadecimal, i, len, number) {
        len = length(binary)
        len += (4 - len % 4) % 4
        binary = left_pad(binary, len, "0")
        hexadecimal = ""
        for (i = len - 3; i >= 1; i -= 4) {
            number = substr(binary, i, 4)
            if (number == "0000") {
                hexadecimal = "0" hexadecimal
            } else if (number == "0001") {
                hexadecimal = "1" hexadecimal
            } else if (number == "0010") {
                hexadecimal = "2" hexadecimal
            } else if (number == "0011") {
                hexadecimal = "3" hexadecimal
            } else if (number == "0100") {
                hexadecimal = "4" hexadecimal
            } else if (number == "0101") {
                hexadecimal = "5" hexadecimal
            } else if (number == "0110") {
                hexadecimal = "6" hexadecimal
            } else if (number == "0111") {
                hexadecimal = "7" hexadecimal
            } else if (number == "1000") {
                hexadecimal = "8" hexadecimal
            } else if (number == "1001") {
                hexadecimal = "9" hexadecimal
            } else if (number == "1010") {
                hexadecimal = "a" hexadecimal
            } else if (number == "1011") {
                hexadecimal = "b" hexadecimal
            } else if (number == "1100") {
                hexadecimal = "c" hexadecimal
            } else if (number == "1101") {
                hexadecimal = "d" hexadecimal
            } else if (number == "1110") {
                hexadecimal = "e" hexadecimal
            } else if (number == "1111") {
                hexadecimal = "f" hexadecimal
            } else {
                return ""
            }
        }
        sub(/^0+/, "", hexadecimal)
        if (hexadecimal == "") {
            hexadecimal = "0"
        }
        hexadecimal = "0x" hexadecimal
        return hexadecimal
    }
' >> "$__awk_tmp"
