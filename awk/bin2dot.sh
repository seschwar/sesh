sesh_load awk
sesh_load awk/bin2hex
sesh_load awk/hex2dot


printf '%s\n' '
    function bin2dot(binary) {
        return hex2dot(bin2hex(binary))
    }
' >> "$__awk_tmp"
