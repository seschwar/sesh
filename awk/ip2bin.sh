sesh_load awk
sesh_load awk/col2bin
sesh_load awk/dot2bin
sesh_load awk/left_pad


printf '%s\n' '
    function ip2bin(address) {
        if (address ~ /:/) {
            address = left_pad(col2bin(address), 128, "0")
        } else {
            address = left_pad(dot2bin(address), 32, "0")
        }
        return address
    }
' >> "$__awk_tmp"
