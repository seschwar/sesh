sesh_load awk


printf '%s\n' '
    function dot2hex(dotted_decimal, _, hexadecimal, i, len, octets) {
        len = split(dotted_decimal, octets, /\./)
        hexadecimal = "0x"
        for (i = 1; i <= len; i++) {
            hexadecimal = hexadecimal sprintf("%02x", octets[i])
        }
        sub(/^0x0+/, "0x", hexadecimal)
        if (hexadecimal == "0x") {
            hexadecimal = "0x0"
        }
        return hexadecimal
    }
' >> "$__awk_tmp"
