sesh_load awk


printf '%s\n' '
    function hex_swap_bytes(little_endian, _, big_endian, i, len) {
        sub(/^0[Xx]/, "", little_endian)
        len = length(little_endian)
        big_endian = "0x"
        for (i = 1; i <= len; i += 2) {
            big_endian = big_endian substr(little_endian, len - i, 2)
        }
        if (big_endian == "0x") {
            big_endian = "0x0"
        }
        return big_endian
    }
' >> "$__awk_tmp"
