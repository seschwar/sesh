sesh_load awk


printf '%s\n' '
    function __initialize_char2int_map(_, i) {
        for (i = 0; i < 256; i++) {
            __char2int_map[sprintf("%c", i)] = i
        }
    }
    BEGIN {
        __initialize_char2int_map()
    }
    function char2int(character) {
        return __char2int_map[character]
    }
' >> "$__awk_tmp"
