sesh_load awk
sesh_load awk/normalize_ipv4
sesh_load awk/normalize_ipv6


printf '%s\n' '
    function normalize_ip(ip_address) {
        if (ip_address ~ /:/) {
            ip_address = normalize_ipv6(ip_address)
        } else {
            ip_address = normalize_ipv4(ip_address)
        }
        return ip_address
    }
' >> "$__awk_tmp"
