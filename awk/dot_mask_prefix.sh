sesh_load awk
sesh_load awk/bin_mask_prefix
sesh_load awk/dot2bin


printf '%s\n' '
    function dot_mask_prefix(address) {
        return bin_mask_prefix(dot2bin(address))
    }
' >> "$__awk_tmp"
