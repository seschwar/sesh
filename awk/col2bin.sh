sesh_load awk
sesh_load awk/col2hex
sesh_load awk/hex2bin


printf '%s\n' '
    function col2bin(colonned_hexadecimal) {
        return hex2bin(col2hex(colonned_hexadecimal))
    }
' >> "$__awk_tmp"
