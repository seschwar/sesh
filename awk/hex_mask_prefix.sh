sesh_load awk
sesh_load awk/bin_mask_prefix
sesh_load awk/hex2bin


printf '%s\n' '
    function hex_mask_prefix(address) {
        return bin_mask_prefix(hex2bin(address))
    }
' >> "$__awk_tmp"
