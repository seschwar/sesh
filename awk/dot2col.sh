sesh_load awk
sesh_load awk/dot2hex
sesh_load awk/hex2col


printf '%s\n' '
    function dot2col(dotted_decimal) {
        return hex2col(dot2hex(dotted_decimal))
    }
' >> "$__awk_tmp"
