sesh_load awk


printf '%s\n' '
    function month_number(name) {
        name = tolower(substr(name, 1, 3))
        if (name == "jan") {
            return 1
        } else if (name == "feb") {
            return 2
        } else if (name == "mar") {
            return 3
        } else if (name == "apr") {
            return 4
        } else if (name == "may") {
            return 5
        } else if (name == "jun") {
            return 6
        } else if (name == "jul") {
            return 7
        } else if (name == "aug") {
            return 8
        } else if (name == "sep") {
            return 9
        } else if (name == "oct") {
            return 10
        } else if (name == "nov") {
            return 11
        } else if (name == "dec") {
            return 12
        } else {
            return 0
        }
    }
' >> "$__awk_tmp"
