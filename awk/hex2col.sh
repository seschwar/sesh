sesh_load awk
sesh_load awk/left_pad


printf '%s\n' '
    function hex2col(hexadecimal) {
        sub(/^0[Xx]/, "", hexadecimal)
        hexadecimal = left_pad(hexadecimal, 32, "0")
        gsub(/..../, "&:", hexadecimal)
        sub(/:$/, "", hexadecimal)
        return tolower(hexadecimal)
    }
' >> "$__awk_tmp"
