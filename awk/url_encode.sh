sesh_load awk
sesh_load awk/char2int


printf '%s\n' '
    function url_encode(string, _, character, i, len) {
        len = length(string)
        for (i = 1; i <= len; i++) {
            if (substr(string, i, 1) !~ /[-.0-9A-Z_a-z~]/) {
                string = sprintf("%s%%%02X%s", substr(string, 1, i - 1), char2int(substr(string, i, 1)), substr(string, i + 1))
                i += 2
                len += 2
            }
        }
        return string
    }
' >> "$__awk_tmp"
