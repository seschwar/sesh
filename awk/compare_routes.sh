sesh_load awk
sesh_load awk/min
sesh_load awk/test_route


printf '%s\n' '
    function compare_routes(destination1, prefix1, metric1, destination2, prefix2, metric2) {
        if (!test_route(destination1, destination2, min(prefix1, prefix2))) {
            return 10
        }
        if (prefix1 < prefix2) {
            return -1
        }
        if (prefix1 == prefix2) {
            return metric2 - metric1
        }
        return 1
    }
' >> "$__awk_tmp"
