sesh_load awk


printf '%s\n' '
    function hex2bin(hexadecimal, _, binary, i, len, number) {
        hexadecimal = tolower(hexadecimal)
        sub(/^(0x)?0*/, "", hexadecimal)
        len = length(hexadecimal)
        binary = ""
        for (i = len; i >= 1; i--) {
            number = substr(hexadecimal, i, 1)
            if (number == "0") {
                binary = "0000" binary
            } else if (number == "1") {
                binary = "0001" binary
            } else if (number == "2") {
                binary = "0010" binary
            } else if (number == "3") {
                binary = "0011" binary
            } else if (number == "4") {
                binary = "0100" binary
            } else if (number == "5") {
                binary = "0101" binary
            } else if (number == "6") {
                binary = "0110" binary
            } else if (number == "7") {
                binary = "0111" binary
            } else if (number == "8") {
                binary = "1000" binary
            } else if (number == "9") {
                binary = "1001" binary
            } else if (number == "a") {
                binary = "1010" binary
            } else if (number == "b") {
                binary = "1011" binary
            } else if (number == "c") {
                binary = "1100" binary
            } else if (number == "d") {
                binary = "1101" binary
            } else if (number == "e") {
                binary = "1110" binary
            } else if (number == "f") {
                binary = "1111" binary
            } else {
                return ""
            }
        }
        sub(/^0+/, "", binary)
        if (binary == "") {
            binary = "0"
        }
        return binary
    }
' >> "$__awk_tmp"
