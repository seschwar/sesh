str_strip_end() # BRACKET_EXPRESSION (STRING...|< STRING) > STRING
# Strip characters matching the basic regular expression
# BRACKET_EXPRESSION from the end of the STRINGs.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_03_05
{
    local expression="$1"
    shift 1

    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | sed "s/[$expression]*\$//"
}
