sh_is_function() # (NAME...|< NAME)
# Test whether the NAMEs refer to shell functions.
{
    if test "$#" -gt 0
    then
        local name
        for name in "$@"
        do
            case "$(command -v "$name" || :)" in
                (''|'alias '*|/*)
                    return 1
                    ;;
                (*)
                    (
                        set +e
                        unset -f "$name"
                        ! command -v "$name"
                    ) > /dev/null 2>&1 || return 1
                    ;;
            esac
        done
    else
        local name
        while IFS= read -r name
        do
            case "$(command -v "$name" || :)" in
                (''|'alias '*|/*)
                    return 1
                    ;;
                (*)
                    (
                        set +e
                        unset -f "$name"
                        ! command -v "$name"
                    ) > /dev/null 2>&1 || return 1
                    ;;
            esac
        done
    fi
    return 0
}
