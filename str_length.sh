str_length() # (STRING...|< STRING) > LENGTH
# Output the LENGTH of the STRINGs.
{
    if test "$#" -gt 0
    then
        local string
        for string in "$@"
        do
            echo "${#string}"
        done
    else
        local string
        while IFS= read -r string
        do
            echo "${#string}"
        done
    fi
}
