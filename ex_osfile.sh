sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_OSFILE=72


ex_osfile() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_OSFILE after printing the error MESSAGE.
{
    err "$EX_OSFILE" "$@"
}
