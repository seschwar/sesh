sesh_load awk
sesh_load awk/url_decode


url_decode() # (ENCODED...|< ENCODED) > STRING
# Decode the RFC 3986 percent-ENCODED strings.
#
# https://en.wikipedia.org/wiki/Percent-encoding
# https://tools.ietf.org/html/rfc3986
{
    if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | LC_ALL=C awk '{ print url_decode($0) }'
}
