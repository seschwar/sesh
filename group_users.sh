sesh_load ex_unavailable
sesh_load sh_is_command


group_users() # GROUP... > USER
# Output the USERs of the GROUPs.
{
    if sh_is_command dscacheutil
    then
        local group
        for group in "$@"
        do
            if expr "$group" : '[0-9]\{1,\}$' > /dev/null
            then
                dscacheutil -q group -a gid "$group"
            else
                dscacheutil -q group -a name "$group"
            fi
        done | sed -n 's/^users: //p'
    elif sh_is_command getent
    then
        getent group "$@" | cut -d : -f 4
    else
        return "$EX_UNAVAILABLE"
    fi
}
