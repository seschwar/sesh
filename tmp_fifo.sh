sesh_load tmp_dir


tmp_fifo() # [TEMPLATE] > PATH
# Create a temporary fifo.
#
# The fifo is automatically deleted on program termination.
{
    local template="${1:-XXXXXXXX}"
    local fifo; fifo=$(tmp_dir "$template")/$template
    mkfifo "$fifo"
    printf '%s\n' "$fifo"
}
