sesh_load str_strip_end


str_strip_space_end() # (STRING...|< STRING) > STRING
# Strip all whitespace from the end of the STRINGs.
{
    str_strip_end '[:space:]' "$@"
}
