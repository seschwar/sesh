sesh_doc() # [NAME...] > DOCUMENTATION
# Print the documentation for the NAMEs (or all available).
{
    if test "$#" -gt 0
    then
        local name
        for name in "$@"
        do
            local dir
            local IFS=:
            for dir in $SESH_PATH
            do
                if test -z "$dir"
                then
                    continue
                fi
                local file="$dir/$name.sh"
                if test -e "$file"
                then
                    break
                fi
            done
            printf '%s\0' "$file"
        done
    else
        local IFS=:
        find $SESH_PATH -type f -name '*.sh' -print0
    fi | xargs -0 sed -ne '
        /^[^[:space:](]\{1,\}[[:space:]]*()/,/^[({]/ {
            s/^[({].*$//
            p
        }
    ' --
}
