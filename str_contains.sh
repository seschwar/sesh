str_contains() # INFIX (STRING...|< STRING) 
# Test whether INFIX is contained in the STRINGs.
{
    local infix="$1"
    shift 1

    if test "$#" -gt 0
    then
        local string
        for string in "$@"
        do
            case "$string" in
                (*"$infix"*)
                    ;;
                (*)
                    return 1
                    ;;
            esac
        done
    else
        local string
        while IFS= read -r string
        do
            case "$string" in
                (*"$infix"*)
                    ;;
                (*)
                    return 1
                    ;;
            esac
        done
    fi
    return 0
}
