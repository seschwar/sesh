sesh_load awk


torrent_created_by() # TORRENT > CREATOR
# Get the TORRENT file's CREATOR.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "GENERAL" && $1 == "Created" && $2 == "by:" {
            sub(/^[^:]+: /, "")
            print
        }
    '
}
