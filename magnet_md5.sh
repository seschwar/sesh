sesh_load magnet_exact_topic


magnet_md5() # MAGNET_URI > MESSAGE_DIGEST_5_HASH
# Get the MAGNET_URI's exact topic's MESSAGE_DIGEST_5_HASHes.
#
# https://en.wikipedia.org/wiki/MD5
{
    magnet_exact_topic "$@" | sed -n 's/^urn:md5://p'
}
