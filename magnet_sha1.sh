sesh_load magnet_exact_topic


magnet_sha1() # MAGNET_URI > SECURE_HASH_ALGORITHM_1_HASH
# Get the MAGNET_URI's exact topic's SECURE_HASH_ALGORITHM_1_HASHes.
#
# https://en.wikipedia.org/wiki/SHA-1
{
    magnet_exact_topic "$@" | sed -n 's/^urn:sha1://p'
}
