sesh_load magnet_exact_topic


magnet_tth() # MAGNET_URI > TIGER_THREE_HASH
# Get the MAGNET_URI's exact topic's TIGER_TREE_HASHes.
#
# https://en.wikipedia.org/wiki/Merkle_tree#Tiger_tree_hash
{
    magnet_exact_topic "$@" | sed -n 's/^urn:tree:tiger://p'
}
