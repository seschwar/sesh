sesh_load magnet_address_tracker
sesh_load magnet_btih
sesh_load tmp_fifo
sesh_load torrent_blacklisted_trackers
sesh_load torrent_public_trackers
sesh_load url_encode


magnet_update_trackers() # MAGNET_URI > MAGNET_URI
# Update address trackers in MAGNET_URI with public trackers.
{
    local blacklist; blacklist=$(tmp_fifo)
    local magnet; magnet=$(tmp_fifo)
    local pids=
    local public; public=$(tmp_fifo)
    local trackers; trackers=$(tmp_fifo)
    local uri="$1"

    magnet_address_tracker "$uri" > "$magnet" &
    pids="$pids $!"
    torrent_blacklisted_trackers > "$blacklist" &
    pids="$pids $!"
    torrent_public_trackers "$(magnet_btih "$uri")" > "$public" &
    pids="$pids $!"
    sort -u -- "$magnet" "$public" \
        | comm -23 - "$blacklist" \
        | url_encode \
        | sed 's/^/tr=/' \
        > "$trackers" &
    pids="$pids $!"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed '/^tr=/d' \
        | cat -- - "$trackers" \
        | tr '\n' '&' \
        | sed 's/&/?/; s/&$//;'
    wait $pids
}
