sesh_load awk
sesh_load awk/normalize_ip
sesh_load awk/normalize_ipv4
sesh_load awk/normalize_ipv6
sesh_load sh_is_command
sesh_load sig_timeout
sesh_load tmp_fifo


net_resolve() # HOSTNAME > ADDRESS...
# Resolve the HOSTNAME to IP ADDRESSes.
{
    {
        local hostname="$1"

        if expr "$hostname" : '[.[:digit:]]*$' > /dev/null
        then
            echo "$hostname" | awk '{ print normalize_ipv4($0); }'
        elif expr "$hostname" : '[:.[:xdigit:]]*$' > /dev/null
        then
            echo "$hostname" | awk '{ print normalize_ipv6($0); }'
        fi
        if sh_is_command dig
        then
            {
                dig "$hostname" IN A
                dig "$hostname" IN AAAA
            } | awk '
                /^;/ {
                    next
                }
                /\tIN\tA\t/ {
                    print normalize_ipv4($5)
                }
                /\tIN\tAAAA\t/ {
                    print normalize_ipv6($5)
                }
            '
        fi
        if sh_is_command drill
        then
            {
                drill -- "$hostname" IN A
                drill -- "$hostname" IN AAAA
            } | awk '
                /^;/ {
                    next
                }
                /\tIN\tA\t/ {
                    print normalize_ipv4($5)
                }
                /\tIN\tAAAA\t/ {
                    print normalize_ipv6($5)
                }
            '
        fi
        if sh_is_command dscacheutil
        then
            dscacheutil -q host -a name "$hostname" | awk '
                $1 == "ip_address:" {
                    print normalize_ipv4($2)
                }
                $1 == "ip6_address:" {
                    print normalize_ipv6($2)
                }
            '
        fi
        if sh_is_command dns-sd
        then
            ## For some reason dns-sd doesn't terminate if the
            ## reader of its output quits.  Therefore we use a
            ## short timeout.
            local ipv4_stdout; ipv4_stdout=$(tmp_fifo)
            local ipv6_stdout; ipv6_stdout=$(tmp_fifo)
            local pids=
            sig_timeout 1 TERM dns-sd -q "$hostname" A IN > "$ipv4_stdout" &
            pids="$pids $!"
            sig_timeout 1 TERM dns-sd -q "$hostname" AAAA IN > "$ipv6_stdout" &
            pids="$pids $!"
            paste -d '\n' -- "$ipv4_stdout" "$ipv6_stdout" | awk '
                $2 == "Add" && !/No Such Record/ {
                    print normalize_ip($8)
                }
            ' &
            pids="$pids $!"
            wait $pids
        fi
        if sh_is_command getent && getent ahostsv4 localhost > /dev/null
        then
            getent ahostsv4 "$hostname" \
                | awk '{ print normalize_ipv4($1); }'
            getent ahostsv6 "$hostname" \
                | awk '{ print normalize_ipv6($1); }'
        fi
        if sh_is_command getent
        then
            getent hosts "$hostname" \
                | awk ' { print normalize_ip($1); }'
        fi
        if sh_is_command host
        then
            {
                host -c IN -t A -v -- "$hostname"
                host -c IN -t AAAA -v -- "$hostname"
            } | awk '
                /^;/ {
                    next
                }
                /\tIN\tA\t/ {
                    print normalize_ipv4($5)
                }
                /\tIN\tAAAA\t/ {
                    print normalize_ipv6($5)
                }
            '
        fi
        if test -f /etc/hosts
        then
            awk -v hostname="$hostname" '
                /^[ 	]*(#.*)?$/ {
                    next
                }
                {
                    for (i = 2; i <= NF; i++) {
                        if ($i == hostname) {
                            print normalize_ip($1)
                        }
                    }
                }
            ' < /etc/hosts
        fi
    } | sort -u
}
