sh_is_executable() # (NAME...|< NAME)
# Test whether the NAMEs refer to executables.
{
    if test "$#" -gt 0
    then
        local name
        for name in "$@"
        do
            case "$(command -v "$name" || :)" in
                (/*)
                    ;;
                (*)
                    return 1
                    ;;
            esac
        done
    else
        local name
        while IFS= read -r name
        do
            case "$(command -v "$name" || :)" in
                (/*)
                    ;;
                (*)
                    return 1
                    ;;
            esac
        done
    fi
    return 0
}
