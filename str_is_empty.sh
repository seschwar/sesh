str_is_empty() # (STRING...|< STRING)
# Test whether the STRINGs are empty.
{
    if test "$#" -gt 0
    then
        local string
        for string in "$@"
        do
            if test -n "$string"
            then
                return 1
            fi
        done
    else
        local string
        while IFS= read -r string
        do
            if test -n "$string"
            then
                return 1
            fi
        done
    fi
    return 0
}
