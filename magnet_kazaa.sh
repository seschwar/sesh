sesh_load magnet_exact_topic


magnet_kazaa() # MAGNET_URI > KAZAA_HASH
# Get the MAGNET_URI's exact topic's KAZAA_HASHes.
#
# https://en.wikipedia.org/wiki/Kazaa
{
    magnet_exact_topic "$@" | sed -n 's/^urn:kzhash://p'
}
