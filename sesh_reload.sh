sesh_load sesh_load
sesh_load sesh_unload


sesh_reload() # NAMEs...
# Un- and then re-load the NAMEs.
{
    sesh_unload "$@"
    sesh_load "$@"
}
