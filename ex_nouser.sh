sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_NOUSER=67


ex_nouser() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_NOUSER after printing the error MESSAGE.
{
    err "$EX_NOUSER" "$@"
}
