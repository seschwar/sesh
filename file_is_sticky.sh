file_is_sticky() # (FILE...|< FILE)
# Test whether the FILEs have their sticky bit set.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -k "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -k "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
