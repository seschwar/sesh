sesh_load fd_validate


fd_clobber() # FD PATH [COMMAND [ARGUMENT...]]
# Redirect FD overwriting PATH for self or COMMAND.
{
    local fd="$1"
    local path="$2"
    shift 2

    fd_validate "$fd"
    if test "$#" -eq 0
    then
        set -- exec
    fi
    eval "\"\$@\" $fd>| \"\$path\""
}
