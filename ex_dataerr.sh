sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_DATAERR=65


ex_dataerr() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_DATAERR after printing the error MESSAGE.
{
    err "$EX_DATAERR" "$@"
}
