sesh_load warn


err() # STATUS (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with STATUS after printing the error MESSAGE.
#
# https://www.freebsd.org/cgi/man.cgi?query=err&sektion=3
{
    local status="$1"
    shift 1

    warn "$@"
    exit "$status"
}
