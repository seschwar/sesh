bre_match() # REGULAR_EXPRESSION (STRING...|< STRING)
# Test whether the basic REGULAR_EXPRESSION matches the STRING.
#
# https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html#tag_09_03
{
    local regex="$1"
    shift 1

    ! if test "$#" -gt 0
    then
        printf '%s\n' "$@"
    else
        cat
    fi | grep -qv -- "$regex"
}
