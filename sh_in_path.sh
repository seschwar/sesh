sh_in_path() # (NAME...|< NAME)
# Test whether the NAMEs are in PATH regardless of whether they are
# currently shadowed by shell aliases, builtins of functions.
{
    if test "$#" -gt 0
    then
        local name
        for name in "$@"
        do
            local found=false
            local dir
            local IFS=:
            for dir in $PATH
            do
                if test -x "$dir/$name"
                then
                    found=true
                    break
                fi
            done
            if test "$found" = false
            then
                return 1
            fi
        done
    else
        local name
        while IFS= read -r name
        do
            local found=false
            local dir
            local IFS=:
            for dir in $PATH
            do
                if test -x "$dir/$name"
                then
                    found=true
                    break
                fi
            done
            if test "$found" = false
            then
                return 1
            fi
        done
    fi
}
