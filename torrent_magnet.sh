torrent_magnet() # TORRENT > MAGNET_URI
# Get the TORRENT file's MAGNET_URI.
{
    local torrent="$1"
    
    transmission-show --magnet -- "$torrent"
}
