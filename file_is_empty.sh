file_is_empty() # (FILE...|< FILE)
# Test whether the FILEs are empty.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if test -s "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if test -s "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
