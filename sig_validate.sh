sesh_load sig_list
sesh_load warn


sig_validate() # SIGNAL...
# Validate the names of the SIGNALs.
{
    local signals; signals=$(sig_list)

    local argument
    for argument in "$@"
    do
        local signal
        local IFS='
        '
        for signal in $signals
        do
            if test "$argument" = "$signal"
            then
                continue 2
            fi
        done
        warn "invalid signal name: $argument"
        return 1
    done
}
