and() # COMMAND [ARGUMENT...]
# Run the COMMAND if the most recent pipeline's exit status code is
# truthy.
{
    (exit "$?";) && "$@"
}
