sesh_load awk


torrent_files() # TORRENT > FILES
# Get the TORRENT file's FILES.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "FILES" {
            sub(/^ +/, "")
            print
        }
    '
}
