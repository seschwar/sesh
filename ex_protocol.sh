sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_PROTOCOL=76


ex_protocol() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_PROTOCOL after printing the error MESSAGE.
{
    err "$EX_PROTOCOL" "$@"
}
