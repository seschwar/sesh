user_uid() # [USER...] > UID
# Output the UIDs of the USERs or the current user.
{
    if test "$#" -eq 0
    then
        id -u
    else
        local user
        for user in "$@"
        do
            id -u "$user"
        done
    fi
}
