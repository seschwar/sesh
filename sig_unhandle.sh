sesh_load sig
sesh_load sig_validate


sig_unhandle() # HANDLER SIGNAL...
# Remove a HANDLER for the SIGNALs in the current (sub-)shell.
{
    local handler="$1"
    shift 1

    sig_validate "$@"
    local pid; pid=$(exec sh -c 'echo "$PPID"')
    local signal
    for signal in "$@"
    do
        eval "local handlers=\"\${__sig_handlers_${signal}_${pid}:-:}\""
        case "$handlers" in
            ("$handler")
                eval "__sig_handlers_${signal}_${pid}=:"
                ;;
            ("$handler;"*)
                eval "__sig_handlers_${signal}_${pid}=\${handlers#\"\$handler;\"}"
                ;;
            (*";$handler;"*)
                eval "__sig_handlers_${signal}_${pid}=\${handlers%\";\$handler;\"*};\${handlers#*\";\$handler;\"}"
                ;;
            (*";$handler")
                eval "__sig_handlers_${signal}_${pid}=\${handlers%\";\$handler\"}"
                ;;
        esac
    done
    __sig_trap
}
