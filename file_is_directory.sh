file_is_directory() # (FILE...|< FILE)
# Test whether the FILEs are directories.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -d "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -d "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
