sesh_load path_canonical
sesh_load path_validate


path_absolute() # BASE PATH > ABSOLUTE_PATH
# Output the absolute PATH with respect to BASE (or PWD).
#
# Example: path_absolute /foo bar/qux -> /foo/bar/qux
{
    local base="$1"
    local path="$2"
    path_validate "$base" "$path"
    case "$base" in
        (/*)
            ;;
        (*)
            base=${PWD:-$(pwd)}/$base
            ;;
    esac
    case "$path" in
        (/*)
            path_canonical "$path"
            ;;
        (*)
            path_canonical "$base/$path"
            ;;
    esac
}
