sesh_load awk
sesh_load awk/compare_routes
sesh_load awk/normalize_ipv4
sesh_load awk/normalize_ipv6
sesh_load awk/test_route
sesh_load net_interfaces
sesh_load net_routes
sesh_load sh_is_command
sesh_load tmp_fifo


net_source() # DESTINATION_ADDRESS > SOURCE_ADDRESS
# Output the SOURCE_ADDRESS for the DESTINATION_ADDRESS.
{
    # TODO determine NATted SOURCE_ADDRESS when DESTINATION_ADDRESS is public
    local destination="$1"
    local family=inet

    if expr "$destination" : '[^:]*:' > /dev/null
    then
        family=inet6
    fi

    if
        sh_is_command ip \
        && test "$destination" != 0.0.0.0 \
        && test "$destination" != 0000:0000:0000:0000:0000:0000:0000:0000
    then
        ip -oneline route get to "$destination" | awk '
            {
                for (i = 1; i <= NF; i++) {
                    if ($i == "src") {
                        print $(i + 1)
                        break
                    }
                }
            }
        '
    elif sh_is_command route && route -6nv get localhost > /dev/null 2>&1 && test "$family" = inet6
    then
        route -6nv get "$destination" | awk '{ address = $NF } END { print address }'
    elif sh_is_command route && route -4nv get localhost > /dev/null 2>&1 && test "$family" = inet
    then
        route -4nv get "$destination" | awk '{ address = $NF } END { print address }'
    elif sh_is_command route && route -nv get localhost > /dev/null 2>&1 && test "$family" = inet
    then
        route -nv get "$destination" | awk '{ address = $NF } END { print address }'
    else
        local interfaces; interfaces=$(tmp_fifo)
        local routes; routes=$(tmp_fifo)
        local pids=
        net_interfaces | awk -v family="$family" '$2 == family' | sort -k 1 > "$interfaces" &
        pids="$pids $!"
        net_routes | awk -v family="$family" '$1 == family' | sort -k 6 > "$routes" &
        pids="$pids $!"
        join -1 1 -2 6 "$interfaces" "$routes" \
            | awk -v address="$destination" -v source_family="$family" '
                BEGIN {
                    source_address = ""
                    source_destination = ""
                    source_metric = 0
                    source_prefix = 0
                }
                {
                    interface = $1
                    family = $2
                    source = $3
                    prefix = $4
                    family = $5
                    destination = $6
                    prefix = $7
                    metric = $8
                    if ( \
                        test_route(address, destination, prefix) \
                        && test_route(source, destination, prefix) \
                        && ( \
                            source_address == "" \
                            || compare_routes(destination, prefix, metric, source_destination, source_prefix, source_metric) > 0 \
                        ) \
                    ) {
                        source_address = source
                        source_destination = destination
                        source_metric = metric
                        source_prefix = prefix
                    }
                }
                END {
                    if (source_address == "") {
                        exit 1
                    }
                    if (source_family == "inet6") {
                        source_address = normalize_ipv6(source_address)
                    } else {
                        source_address = normalize_ipv4(source_address)
                    }
                    print source_address
                }
            '
        wait $pids
    fi
}
