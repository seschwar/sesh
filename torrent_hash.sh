sesh_load awk


torrent_hash() # TORRENT > HASH
# Get the TORRENT file's BitTorrent Info HASH.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "GENERAL" && $1 == "Hash:" {
            sub(/^[^:]+: /, "")
            print
        }
    '
}
