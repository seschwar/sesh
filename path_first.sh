sesh_load path_validate


path_first() # PATH > FIRST
# Output the FIRST component of the PATH.
#
# Example: path_first foo/bar/qux -> foo
{
    local path="$1"
    path_validate "$path"
    case "$path" in
        (/*)
            path=/
            ;;
        (*/*)
            path=${path%%/*}
            ;;
        (*)
            ;;
    esac
    printf '%s\n' "$path"
}
