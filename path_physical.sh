sesh_load path_absolute
sesh_load path_butlast
sesh_load path_last
sesh_load path_validate


path_physical() # PATH > PHYSICAL_PATH
# Output the absolute PATH with all symbolic links resolved.
{
    local path="$1"
    path_validate "$path"
    path=$(path_absolute . "$path")
    local directory="$path"
    local file=
    while test "$directory" != /
    do
        ## If there is a symlink loop this will never terminate.
        while test -L "$directory"
        do
            directory=$(path_absolute "$(path_butlast "$directory")" "$(readlink -- "$directory")")
        done
        file=$(path_last "$directory")/$file
        file=${file#/}
        file=${file%/}
        directory=$(path_butlast "$directory")
    done
    printf '%s\n' "$directory$file"
}
