sesh_load sig
sesh_load sig_validate


sig_reset() # SIGNAL...
# Clear the handlers for the SIGNALs in the current (sub-)shell.
{
    sig_validate "$@"
    local pid; pid=$(exec sh -c 'echo "$PPID"')
    local signal
    for signal in "$@"
    do
        eval "__sig_handlers_${signal}_${pid}=:"
    done
    __sig_trap
}
