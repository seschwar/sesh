sesh_load str_strip_start


str_strip_space_start() # (STRING...|< STRING) > STRING
# Strip all whitespace from the start of the STRINGs.
{
    str_strip_start '[:space:]' "$@"
}
