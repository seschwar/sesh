sesh_load ex_unavailable
sesh_load sh_is_command
sesh_load user_name


user_gecos() # [USER...] > GECOS
# Output the GECOS fields of the USERs or the current user.
{
    if test "$#" -eq 0
    then
        set -- "$(user_name)"
    fi
    if sh_is_command dscacheutil
    then
        local user
        for user in "$@"
        do
            if expr "$user" : '[0-9]\{1,\}$' > /dev/null
            then
                dscacheutil -q user -a uid "$user"
            else
                dscacheutil -q user -a name "$user"
            fi
        done | sed -n 's/^gecos: //p'
    elif sh_is_command getent
    then
        getent passwd "$@" | cut -d : -f 5
    else
        return "$EX_UNAVAILABLE"
    fi
}
