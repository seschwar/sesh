sh_function() # NAME VALUE
# Define the shell function NAME with the function body VALUE.
{
    local name="$1"
    local value="$2"
    eval "$name() $value"
}
