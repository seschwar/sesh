torrent_validate() # TORRENT
# Validate the TORRENT file.
{
    local torrent="$1"

    transmission-show -- "$torrent" > /dev/null
}
