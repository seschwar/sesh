sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_UNAVAILABLE=69


ex_unavailable() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_UNAVAILABLE after printing the error MESSAGE.
{
    err "$EX_UNAVAILABLE" "$@"
}
