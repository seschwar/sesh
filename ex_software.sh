sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_SOFTWARE=70


ex_software() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_SOFTWARE after printing the error MESSAGE.
{
    err "$EX_SOFTWARE" "$@"
}
