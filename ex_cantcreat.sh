sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_CANTCREAT=73


ex_cantcreat() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_CANTCREAT after printing the error MESSAGE.
{
    err "$EX_CANTCREAT" "$@"
}
