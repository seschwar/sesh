sesh_load path_validate
sesh_load user_home


path_expand() # PATH > EXPANDED_PATH
# Perform tile expansion on the PATHs.
#
# Example: path_expand ~/bar -> /home/foo/bar
# Example: path_expand ~foo/bar -> /home/foo/bar
{
    local path="$1"
    path_validate "$path"
    case "$path" in
        ('~'*)
            local user="$path"
            user=${user#'~'}
            user=${user%%/*}
            path=$(user_home $user)${path#"~$user"}
            ;;
        (*)
            ;;
    esac
    printf '%s\n' "$path"
}
