str_starts() # PREFIX (STRING...|< STRING) 
# Test whether PREFIX is at the start of the STRINGs.
{
    local prefix="$1"
    shift 1

    if test "$#" -gt 0
    then
        local string
        for string in "$@"
        do
            case "$string" in
                ("$prefix"*)
                    ;;
                (*)
                    return 1
                    ;;
            esac
        done
    else
        local string
        while IFS= read -r string
        do
            case "$string" in
                ("$prefix"*)
                    ;;
                (*)
                    return 1
                    ;;
            esac
        done
    fi
    return 0
}
