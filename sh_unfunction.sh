sh_unfunction() # NAME
# Undefine the shell function NAME.
{
    local name="$1"
    unset -f "$name"
}
