sesh_load awk
sesh_load awk/compare_routes
sesh_load awk/test_route
sesh_load net_routes


net_route() # ROUTED_ADDRESS... > ADDRESS_FAMILY ADDRESS PREFIX_LENGTH METRIC GATEWAY INTERFACE ROUTED_ADDRESS
# Output the route for the given ROUTED_ADDRESSes.
{
    net_routes | awk -v arguments="$*" '
        BEGIN {
            split(arguments, addresses)
        }
        {
            family = $1
            destination = $2
            prefix = $3
            metric = $4
            gateway = $5
            interface = $6
            for (i in addresses) {
                if ( \
                    test_route(addresses[i], destination, prefix) \
                    && ( \
                        routes[addresses[i], "family"] == "" \
                        || compare_routes(destination, prefix, metric, routes[addresses[i], "destination"], routes[addresses[i], "prefix"], routes[addresses[i], "metric"]) > 0 \
                    ) \
                ) {
                    routes[addresses[i], "family"] = family
                    routes[addresses[i], "destination"] = destination
                    routes[addresses[i], "gateway"] = gateway
                    routes[addresses[i], "interface"] = interface
                    routes[addresses[i], "metric"] = metric
                    routes[addresses[i], "prefix"] = prefix
                }
            }
        }
        END {
            for (i in addresses) {
                if ((addresses[i], "destination") in routes) {
                    print \
                        routes[addresses[i], "family"], \
                        routes[addresses[i], "destination"], \
                        routes[addresses[i], "prefix"], \
                        routes[addresses[i], "metric"], \
                        routes[addresses[i], "gateway"], \
                        routes[addresses[i], "interface"], \
                        addresses[i]
                }
            }
        }
    '
}
