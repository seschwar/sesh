# About

[`sesh`](https://gitlab.com/seschwar/sesh) ([**se**schwar](https://gitlab.com/seschwar)'s **sh**ell library) is a utility library of shell functions.
It mostly provides a consistent interface for already existing functionality.
It is available under the [ISC License](LICENSE.txt).

# Documentation

Each function lives in its own file and includes documentation.
The documentation for a function can also be displayed [`sesh_doc`] using the function.
See below about information on how to invoke it.

# Usage

Functions can also be loaded from multiple directories by specifying them in the environment variable `SESH_PATH`.
Should a function of the same name exist in multiple directories the one from the directory occurring first in `SESH_PATH` is used.

There a four ways of using [`sesh`](https://gitlab.com/seschwar/sesh).

## Define `SESH_PATH` and Source [`sesh_load`]

Define `SESH_PATH` appropriately and source [`sesh_load`].
Then you can use [`sesh_load`] to individually source the functions from the library you want to use.

    #!/bin/sh
    SESH_PATH=/path/to/directory/containing/sesh_load:/another/directory/with/custom/functions
    . "${SESH_PATH%%:*}/sesh_load.sh"
    sesh_load url_encode
    url_encode 'Hello, world!'

## Source [`sesh`] from `PATH`

To ease the initial sourcing you can place the script [`sesh`] in your `PATH`.
Ensure that the file is still called `sesh`.

    ln -s /path/to/sesh ~/bin/sesh
    
Then you can use:
    
    #!/bin/sh
    . sesh
    sesh_load url_encode
    url_encode 'Hello, world!'

If `SESH_PATH` was undefined [`sesh`] will use its realpath's directory path.

## Execute [`sesh`]

You can also directly invoke a function from the library.
To do this execute [`sesh`] instead of sourcing it.
Pass the function call as the arguments.

    sesh url_encode 'Hello, world!'

## Include output of [`sesh_dump`]

In some situation it may be desirable to just include a copy of the source code of the needed functions with your script.
The function [`sesh_dump`] exists just for that use case.
It outputs the source code of the specified functions and their dependencies, which can be easily copied into another script.

    sesh sesh_dump url_encode >> my/existing/script.sh
    
Inside the script sourcing [`sesh_load`] and loading individual function with [`sesh_load`] is no longer necessary.

[`sesh`]: sesh
[`sesh_doc`]: sesh_doc.sh
[`sesh_dump`]: sesh_dump.sh
[`sesh_load`]: sesh_load.sh

# Related Work

- [pure bash bible](https://github.com/dylanaraps/pure-bash-bible)
- [pure sh bible](https://github.com/dylanaraps/pure-sh-bible)
