sesh_load url_decode


magnet_acceptable_source() # MAGNET_URI > ACCEPTABLE_SOURCE
# Get the MAGNET_URI's ACCEPTABLE_SOURCEs.
{
    local uri="$1"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed -n 's/^as=//p' \
        | url_decode
}
