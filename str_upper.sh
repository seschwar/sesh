sesh_load str_translate


str_upper() # (STRING...|< STRING) > STRING
# Translate all characters in the STRINGs to upper case.
{
    str_translate '[:lower:]' '[:upper:]' "$@"
}
