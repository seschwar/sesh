file_matches_egid() # (FILE...|< FILE)
# Test whether the FILEs GIDs match the effective GID of the current
# process.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -G "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -G "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
