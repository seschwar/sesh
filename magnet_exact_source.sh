sesh_load url_decode


magnet_exact_source() # MAGNET_URI > EXACT_SOURCE
# Get the MAGNET_URI's EXACT_SOURCEs.
{
    local uri="$1"

    printf '%s\n' "$uri" \
        | tr '?&' '\n' \
        | sed -n 's/^xs=//p' \
        | url_decode
}
