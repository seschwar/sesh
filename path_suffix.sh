sesh_load path_extension


path_suffix() # PATH > FILENAME_SUFFIX
# Output the SUFFIX (including the ".") of the FILENAME of the PATH.
#
# Example: path_stem foo/bar.qux -> .qux
{
    local extension
    local path="$1"

    extension=$(path_extension "$path")
    if test -n "$extension"
    then
        echo ".$extension"
    fi
}
