sesh_load awk
sesh_load awk/re_quote
sesh_load tmp_fifo


col_map() # DELIMITER COLUMN COMMAND [ARGUMENT...]
# Feed each field of the 1-based COLUMN number as a line to the
# COMMAND's standard input and replace it with the output.
{
    local column="$2"
    local delimiter="$1"
    shift 2

    local columns; columns=$(tmp_fifo)
    tee -- "$columns" \
        | cut -d "$delimiter" -f "$column" \
        | "$@" \
        | paste -d "$delimiter" -- - "$columns" \
        | awk -F "$delimiter" -v OFS="$delimiter" -v column="$column" '
            BEGIN {
                FS = re_quote(FS)
            }
            {
                for (i = 2; i < column + 1; i++) {
                    printf "%s%s", $i, OFS
                }
                printf "%s", $1
                for (i = column + 2; i <= NF; i++) {
                    printf "%s%s", OFS, $i
                }
                printf "%s", ORS
            }
        '
}
