sesh_load awk


torrent_piece_count() # TORRENT > COUNT
# Get the TORRENT file's piece COUNT.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "GENERAL" && $1 == "Piece" && $2 == "Count:" {
            sub(/^[^:]+: /, "")
            print
        }
    '
}
