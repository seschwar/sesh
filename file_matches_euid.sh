file_matches_euid() # (FILE...|< FILE)
# Test whether the FILEs UIDs match the effective UID of the current
# process.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -O "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -O "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
