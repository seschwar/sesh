sesh_load awk
sesh_load tmp_file


: "${__torrent_blacklisted_trackers:=$(tmp_file)}"


torrent_blacklisted_trackers() # > TRACKER_URL
# Output publicly blacklisted TRACKER_URLs.
{
    if ! test -s "$__torrent_blacklisted_trackers"
    then
        curl \
            --connect-timeout 10 \
            --fail \
            --location \
            --output - \
            --silent \
            --url https://raw.githubusercontent.com/ngosang/trackerslist/master/blacklist.txt \
            | awk -F ' # ' '{ print $1 }' \
            | sort -u \
            > "$__torrent_blacklisted_trackers"
    fi
    cat -- "$__torrent_blacklisted_trackers"
}
