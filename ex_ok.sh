sesh_load err


# https://www.freebsd.org/cgi/man.cgi?query=sysexits&sektion=3
readonly EX_OK=0


ex_ok() # (MESSAGE...|< MESSAGE) 2> MESSAGE
# Exit with EX_OK after printing the error MESSAGE.
{
    err "$EX_OK" "$@"
}
