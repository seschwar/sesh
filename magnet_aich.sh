sesh_load magnet_exact_topic


magnet_aich() # MAGNET_URI > ADVANCED_INTELLIGENT_CORRUPTION_HANDLER
# Get the MAGNET_URI's exact topic's
# ADVANCED_INTELLIGENT_CORRUPTION_HANDLERs.
#
# https://en.wikipedia.org/wiki/AICH
{
    magnet_exact_topic "$@" | sed -n 's/^urn:aich://p'
}
