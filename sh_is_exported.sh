sh_is_exported() # NAME
# Test whether the variable NAME is exported.
{
    export -p | while IFS= read -r line
    do
        set +e
        case "$line" in
            ("export $1="*)
                return 0
                ;;
            (*)
                false
                ;;
        esac
    done
    return "$?"
}
