sesh_load sig_list


__sig_trap()
# Activate traps of the current (sub-)shell with the current handlers.
#
# All traps except the ones for signals which are ignored by default
# need to be activated to ensure the EXIT trap isn't skipped.  For
# example if the INT trap is left at the default some shell will exit
# without running the EXIT trap.
{
    local pid; pid=$(exec sh -c 'echo "$PPID"')
    local signals; signals=$(sig_list)

    local signal
    local IFS='
    '
    for signal in $signals
    do
        case "$signal" in
            (CHLD|CLD|URG|WINCH)
                eval "local handlers=\"\${__sig_handlers_${signal}_${pid}:-:;SIG_DFL}\""
                ;;
            (*)
                eval "local handlers=\"\${__sig_handlers_${signal}_${pid}:-:}\""
                ;;
        esac
        case "$handlers" in
            (*';SIG_DFL')
                trap -- - "$signal" 2> /dev/null || :
                ;;
            (*';SIG_IGN')
                trap -- '' "$signal" 2> /dev/null || :
                ;;
            (*)
                trap -- "$handlers" "$signal" 2> /dev/null || :
                ;;
        esac
    done
}
