sesh_load awk


torrent_comment() # TORRENT > COMMENT
# Get the TORRENT file's COMMENT.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "GENERAL" && $1 == "Comment:" {
            sub(/^[^:]+: /, "")
            print
        }
    '
}
