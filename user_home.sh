sesh_load ex_unavailable
sesh_load sh_is_command


user_home() # [USER...] > HOME
# Output the HOME directories of the USERs or the current user.
{
    if test "$#" -eq 0
    then
        printf '%s\n' "${HOME:-$(cd && pwd)}"
    else
        if sh_is_command dscacheutil
        then
            local user
            for user in "$@"
            do
                if expr "$user" : '[0-9]\{1,\}$' > /dev/null
                then
                    dscacheutil -q user -a uid "$user"
                else
                    dscacheutil -q user -a name "$user"
                fi
            done | sed -n 's/^dir: //p'
        elif sh_is_command getent
        then
            getent passwd "$@" | cut -d : -f 6
        else
            return "$EX_UNAVAILABLE"
        fi
    fi
}
