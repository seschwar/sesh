# The already sourced files.
: "${__sesh_load_sourced:=sesh_load}"


sesh_load() # NAME...
# Source the NAMEs unless they were sourced already by sesh_load.
{
    local name
    for name in "$@"
    do
        case "$__sesh_load_sourced" in
            ("$name"|"$name "*|*" $name "*|*" $name")
                continue
                ;;
        esac
        local dir
        local IFS=:
        for dir in $SESH_PATH
        do
            if test -z "$dir"
            then
                continue
            fi
            local file="$dir/$name.sh"
            if test -e "$file"
            then
                break
            fi
        done
        . "$file" || return "$?"
        # Recording the name before sourcing would allow circular and
        # recursive loads.  However it would invert the ordering of
        # the function's dependencies, which in turn breaks the output
        # of sesh_dump().
        __sesh_load_sourced="$__sesh_load_sourced $name"
    done
}
