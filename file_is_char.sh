file_is_char() # (FILE...|< FILE)
# Test whether the FILEs are character device files.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -c "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -c "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
