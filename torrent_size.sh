sesh_load awk


torrent_size() # TORRENT > SIZE
# Get the TORRENT file's SIZE.
{
    local torrent="$1"
    
    LC_ALL=C transmission-show -- "$torrent" | awk '
        NF == 0 {
            next
        }
        /^[A-Z]+$/ {
            section = $0
            next
        }
        section == "GENERAL" && $1 == "Total" && $2 == "Size:" {
            sub(/^[^:]+: /, "")
            print
        }
    '
}
