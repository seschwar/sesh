str_ends() # SUFFIX (STRING...|< STRING) 
# Test whether SUFFIX is at the end of the STRINGs.
{
    local suffix="$1"
    shift 1

    if test "$#" -gt 0
    then
        local string
        for string in "$@"
        do
            case "$string" in
                (*"$suffix")
                    ;;
                (*)
                    return 1
                    ;;
            esac
        done
    else
        local string
        while IFS= read -r string
        do
            case "$string" in
                (*"$suffix")
                    ;;
                (*)
                    return 1
                    ;;
            esac
        done
    fi
    return 0
}
