sesh_load fd_validate


fd_is_tty() # (FD...|< FD)
# Test whether the FDs are TTYs.
{
    if test "$#" -gt 0
    then
        local fd
        for fd in "$@"
        do
            fd_validate "$fd"
            if ! test -t "$fd"
            then
                return 1
            fi
        done
    else
        local fd
        while IFS= read -r fd
        do
            fd_validate "$fd"
            if ! test -t "$fd"
            then
                return 1
            fi
        done
    fi
    return 0
}
