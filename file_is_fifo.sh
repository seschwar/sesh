file_is_fifo() # (FILE...|< FILE)
# Test whether the FILEs are FIFOs.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -p "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -p "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
