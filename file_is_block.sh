file_is_block() # (FILE...|< FILE)
# Test whether the FILEs are block device files.
{
    if test "$#" -gt 0
    then
        local file
        for file in "$@"
        do
            if ! test -b "$file"
            then
                return 1
            fi
        done
    else
        local file
        while IFS= read -r file
        do
            if ! test -b "$file"
            then
                return 1
            fi
        done
    fi
    return 0
}
