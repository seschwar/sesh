sesh_load tmp_file


sig_timeout() # SECONDS SIGNAL COMMAND [ARGUMENT...]
# Send SIGNAL to the COMMAND's process after SECONDS.
{
    local seconds="$1"
    local signal="$2"
    shift 2

    sig_validate "$signal"
    local command; command=$(tmp_file)
    local sleep; sleep=$(tmp_file)

    (
        set +e
        "$@"
        status=$?
        IFS= read -r sleep < "$sleep"
        kill -s TERM -- "$sleep"
        exit "$status"
    ) &
    local command_pid="$!"
    echo "$command_pid" > "$command"

    (
        set +e
        sleep "$seconds"
        IFS= read -r command < "$command"
        kill -s "$signal" -- "$command"
        exit 0
    ) &
    local sleep_pid="$!"
    echo "$sleep_pid" > "$sleep"

    wait "$sleep_pid" || :
    wait "$command_pid"
}
